from __future__ import division, print_function
import time, os, sys, glob, numpy as np
import data_generation_class.class_definitions as d_gen_class

if (__name__ == "__main__"):

    placeHolder = sys.argv[1]
    # print ("the placeHolder is", placeHolder)
    targetPDBID = placeHolder.rsplit(",")[0].rsplit("_")[0]
    diffraction_ID = placeHolder.rsplit(",")[0].rsplit("_")[1]
    wavelength = float(placeHolder.rsplit(",")[1])
    substructure_atom_type = placeHolder.rsplit(",")[2].rsplit("/")[0]
    f_prime = placeHolder.rsplit(",")[3]
    if f_prime in ["None", "null"]:
       f_prime = None
    f_double_prime = placeHolder.rsplit(",")[4]
    if f_double_prime in ["None", "null"]:
       f_double_prime = None
    # high_resolution=float(placeHolder.rsplit(",")[2])
    # knownSites=int(placeHolder.rsplit(",")[3])
    sourcePath = os.path.join("/mnt/apollo/homes/ksh40/phassade/inputData/kau_dir", targetPDBID)

    destinationPath = os.path.join("/mnt/apollo/homes/rdo20/MRSAD_Calc/phassade/workingDir")
    runDetails = sys.argv[2]

    if not os.path.exists(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID)):
        os.mkdir(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID))
    os.chdir(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID))

    if not os.path.exists(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID, "phasertng_run")):
        os.mkdir(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID, "phasertng_run"))
    os.chdir(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID, "phasertng_run"))

    if not os.path.exists(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID, "phasertng_run", runDetails)):
      os.mkdir(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID, "phasertng_run", runDetails))
    os.chdir(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID, "phasertng_run", runDetails))

    mtzpath = os.path.join(sourcePath, diffraction_ID, targetPDBID + "_" + diffraction_ID + ".mtz")
    if not os.path.exists(mtzpath):
      sourcePath = os.path.join("/mnt/apollo/homes/rdo20/MRSAD_Calc/phassade/inputData", targetPDBID)
      mtzpath = os.path.join(sourcePath, targetPDBID + "_fw_" + diffraction_ID  + ".mtz")

    object_holder = d_gen_class.Phasing_methods(targetPDBID + "_" + diffraction_ID,
                      os.path.join(sourcePath, targetPDBID + ".pdb"),
                      os.path.join(sourcePath, targetPDBID + ".pdb"),
                      os.path.join(mtzpath),
                      os.path.join(sourcePath, targetPDBID + ".fa"),
                      substructure_atom_type, wavelength)

    currentWorkingDir = os.getcwd()

    ###to run with starting from refined f_double_prime
    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
    errorcode = object_holder.run_phassade(currentWorkingDir, f_prime, f_double_prime)  # to run while providing both f_prime and f_double_prime.  Need to change phasertng.phil and "run_phassade" in put parameter to include both these arguments.
    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
    # object_holder.run_phassade()

    # print("redirectedOutput", targetPDBID + "_" + diffraction_ID)
    sys.exit( errorcode.exit_code())
