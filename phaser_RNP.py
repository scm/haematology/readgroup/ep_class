from __future__ import division, print_function
import time, os, sys, glob, numpy as np
import subprocess as sub
import data_generation_class.class_definitions as d_gen_class

if (__name__ == "__main__"):


    targetPDBID = "1UGM"
    diffraction_ID = "w1"
    targetPDBID_filePath = "/home/ksh40/work/rob_rework/reRuns/positive_control_all_targets/RNP_svn8448/1UGM/1UGM/phaser_self_default/1ugm__self_default.1.pdb"
    targetPDBID_mtz = os.path.join("/home/ksh40/work/rob_rework/reRuns/positive_control_all_targets/RNP_svn8448/1UGM/1UGM/phaser_self_default/1ugm__self_default.1.mtz")
    targetPDBID_seq = "/home/ksh40/work/RNP_work/trial_data/1UGM/1UGM.fa"

    object_holder = d_gen_class.Phasing_methods(targetPDBID + "_" + diffraction_ID,
                                                targetPDBID_filePath,
                                                targetPDBID_filePath,
                                                targetPDBID_mtz,
                                                targetPDBID_seq,
                                                "AX")
    object_holder.runRNP(phasingModelPDBID_filePath="/home/ksh40/work/rob_rework/reRuns/svn_8396/default_05_5A/1UGM__1KJT_A/1ugm.1.pdb",
                         phasingModelPDBID = "1UGM_1KJT_A",
                         eVRMS = 1.0,
                         six_dimensions = None)


    # placeHolder = sys.argv[1]
    # # print ("the placeHolder is", placeHolder)
    # targetPDBID = placeHolder.rsplit(",")[0].rsplit("_")[0]
    # diffraction_ID = placeHolder.rsplit(",")[0].rsplit("_")[1]
    # wavelength = float(placeHolder.rsplit(",")[1])
    # substructure_atom_type = placeHolder.rsplit(",")[2].rsplit("/")[0]
    # f_prime = placeHolder.rsplit(",")[3]
    # f_double_prime = placeHolder.rsplit(",")[4]
    # # high_resolution=float(placeHolder.rsplit(",")[2])
    # # knownSites=int(placeHolder.rsplit(",")[3])
    # print (targetPDBID, diffraction_ID, wavelength, substructure_atom_type, f_prime, f_double_prime)
    # summary = []
    #
    # summary.append(targetPDBID + "_" + diffraction_ID)
    #
    # # sourcePath = os.path.join("/mnt/apollo/homes/ksh40/phassade/inputData/kau_dir", targetPDBID)
    # sourcePath = os.path.join("/mnt/apollo/homes/ksh40/phassade/inputData/kau_dir", targetPDBID)
    #
    # if not os.path.exists(sourcePath):
    #     print("noPath_redirectedOutput", targetPDBID + "_" + diffraction_ID, round(wavelength, 4),
    #           substructure_atom_type)
    # else:
    #     destinationPath = os.path.join("/mnt/apollo/homes/ksh40/phassade/work/workingDir")
    #     runDetails = "15Apr_svn760_strategy2FDP"
    #
    #     if not os.path.exists(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID)):
    #         os.mkdir(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID))
    #         os.chdir(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID))
    #     else:
    #         os.chdir(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID))
    #
    #     if not os.path.exists(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID, "phasertng_run")):
    #         os.mkdir(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID, "phasertng_run"))
    #         os.chdir(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID, "phasertng_run"))
    #     else:
    #         os.chdir(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID, "phasertng_run"))
    #
    #     if not os.path.exists(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID, "phasertng_run", runDetails)):
    #         os.mkdir(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID, "phasertng_run", runDetails))
    #         os.chdir(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID, "phasertng_run", runDetails))
    #     else:
    #         os.chdir(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID, "phasertng_run", runDetails))
    #
    #
    #     with open("phassade.log", 'w') as output:
    #         runPhassade=sub.Popen(
    #                 ['phenix.python', '/mnt/apollo/homes/ksh40/phassade/work/scripts/EP_class/phasertng_runJob.py', placeHolder],
    #                 #shell=True,
    #                # stdin=echo.stdout,
    #                 stdout=output,
    #             )
    #     runPhassade.communicate()
    #
