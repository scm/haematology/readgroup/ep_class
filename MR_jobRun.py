from __future__ import division, print_function
import time, os, sys, glob, numpy as np
from itertools import count
import subprocess as sub
import shutil
from iotbx import crystal_symmetry_from_any
from iotbx.pdb import hierarchy
import functions.functions_library as func_lib
import data_generation_class.class_definitions as d_gen_class
import reflection_data_manipulation.reflection_data_manipulation_functions as data_manipulation

def runTempMRJob(targetPDBID,
    targetPDBID_coordFile,  # fully refined full model
    targetPDBID_coordFile_substructure,  # substructure model
    targetPDBID_mtzFile,
    targetPDBID_seqFile,
    substructure_atom_type, wavelength=None,
    phasingModelPDBID_filePath,
    phasingModelPDBID,
    eVRMS):

    object_holder = d_gen_class.Phasing_methods(targetPDBID,
                                                targetPDBID_coordFile,  # fully refined full model
                                                targetPDBID_coordFile_substructure,  # substructure model
                                                targetPDBID_mtzFile,
                                                targetPDBID_seqFile,
                                                substructure_atom_type, wavelength=None)

    MRsummaryResults = object_holder.runRNP(phasingModelPDBID_filePath, phasingModelPDBID, eVRMS,
                                            six_dimensions=None)
    return MRsummaryResults

if (__name__ == "__main__"):
    runTempMRJob(targetPDBID,
                 targetPDBID_coordFile,  # fully refined full model
                 targetPDBID_coordFile_substructure,  # substructure model
                 targetPDBID_mtzFile,
                 targetPDBID_seqFile,
                 substructure_atom_type, wavelength=None,
                 phasingModelPDBID_filePath,
                 phasingModelPDBID,
                 eVRMS):