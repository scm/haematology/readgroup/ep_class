from __future__ import division, print_function

import os, math
import sys

import numpy as np
from iotbx.pdb import hierarchy
from cctbx.array_family import flex
from cctbx import maptbx, xray
from cctbx import maptbx
from cctbx import miller
from iotbx.file_reader import any_file
import subprocess as sub
from cctbx.maptbx import crystal_gridding
from scipy.spatial import distance
from Bio.PDB import PDBParser


def check_for_disulfide(PDB_textfile):
    """
    calculates number of disulfide bonds
    :param PDB_textfile: input PDB text file
    :return: number of (int) disulfide bonds
    """
    # p = PDBParser()
    # s = p.get_structure('example', PDB_textfile)
    # prot = s.as_protein()
    #
    # for bond in prot.search_ss_bonds():
    #     print (bond)
    pdb_in = hierarchy.input(PDB_textfile)
    obj_pdb = pdb_in.construct_hierarchy()
    selected_atoms = obj_pdb.atom_selection_cache().iselection("name SG")
    if len(selected_atoms) > 0:
        selected_atoms_pdb_hierarchy = pdb_in.hierarchy.select(selected_atoms)
        substructure_xyz_np = np.array(selected_atoms_pdb_hierarchy.atoms().extract_xyz())
        distance_matrix = distance.cdist(substructure_xyz_np, substructure_xyz_np, metric='euclidean')
        # the following line selects the values in the matrix between 2 and 2.1.  As the matrix is
        # symmetrical along the diagonal, only unique values are retained and the length of this array
        # represents the number of disulfide bonds which is returned
        return len(np.unique(distance_matrix[np.where(np.logical_and(distance_matrix > 2.0, distance_matrix < 2.1))]))
    else:
        return 0


def expand_to_p1(PDB_textfile, output_file_name):
    """
    Expands the input PDB file to P1 symmetry

    :param PDB_textfile: Input structure file in PDB file format
    :param output_file_name: Output name that needs to be provided (without suffix ".pdb")
    :return: writes out PDB file expanded to P1 symmetry.
    """
    input_file_name = PDB_textfile
    # print(PDB[0])
    pdb_in = hierarchy.input(file_name=input_file_name)
    expanded_to_p1 = pdb_in.hierarchy.expand_to_p1(crystal_symmetry=pdb_in.crystal_symmetry())
    expanded_to_p1.write_pdb_file(file_name=output_file_name + ".pdb",
                                  crystal_symmetry=pdb_in.crystal_symmetry().cell_equivalent_p1(), append_end=True)

def replace_atomType(PDB_textfile, atomType_toReplace):
    """Replace all the atoms with the given atomtype"""
    outputFileName = str(PDB_textfile.rsplit("/")[-1].rsplit(".")[0]) + "_replacedWith_" + atomType_toReplace + ".pdb"
    print(outputFileName)

    with open(PDB_textfile) as f:
        if len(atomType_toReplace)>1:
            newText = f.read().replace("  S   ", " "+atomType_toReplace+"   ")
        else :
            newText = f.read().replace("  S   ",  "  "+atomType_toReplace+"   ")
    with open(outputFileName, "w") as f:
        f.write(newText)


def write_out_selected_atom_into_PDBfile(PDB_textfile, atomType, output_file_name):
    """
    Greps the selected atom type and write it out into separate PDB textfile.

    :param PDB_textfile:Input PDB textfile
    :param atomType: Atom type of the anomalous scatterer
    :param output_file_name: file name of the output file
    :return: write out PDB file with only the select anomalous atom type
    """
    command_line_arguments1 = " remove=" + "\'" + "not(element " + atomType + ")" + "\'"

    command_line_arguments2 = " output.file_name=" + "\'" + output_file_name + ".pdb" + "\'"

    command_line_arguments3 = "format=PDB"

    with open("pdbtools.log", 'w') as output:
        pdbtools = sub.Popen(
            ['phenix.pdbtools', PDB_textfile, command_line_arguments1, command_line_arguments2,
             command_line_arguments3],
            # shell=True,
            # stdin=echo.stdout,
            stdout=output,
        )
    pdbtools.communicate()


def generic_count_atomNumbers(PDB):
    """
    Return number of atoms in the PDB file
    :param PDB: PDB text file
    :return: Number of atoms in the PDB file
    """
    pdb_in = hierarchy.input(PDB)
    obj_pdb = pdb_in.construct_hierarchy()
    selected_atoms = obj_pdb.atom_selection_cache().iselection("element *")
    number_of_atoms = 0
    for f in selected_atoms:
        number_of_atoms = number_of_atoms + 1
    return number_of_atoms


def calculate_total_occupancy(PDB, atomType=None):
    selected_atoms_occupancy = selected_atoms_occupancy_squared_sum = 0
    element_array = []
    element_array_np = []
    first = True

    pdb_in = hierarchy.input(PDB)
    symm = pdb_in.crystal_symmetry()
    obj_pdb = pdb_in.construct_hierarchy()

    ### if atomType is selected then return only selected atomType occupancy audit
    if atomType is not None:
        selected_atoms = obj_pdb.atom_selection_cache().iselection("element " + str(atomType) + " ")
        # not_selected_atoms=obj_pdb.atom_selection_cache().iselection("not element "+str(atomType)+" ")

        number_of_atoms = 0

        for f in selected_atoms:
            # print(float(obj_pdb.atoms()[f].occ))
            # the following line calculates the sum of squared occupancy
            selected_atoms_occupancy += (float(obj_pdb.atoms()[f].occ))
            selected_atoms_occupancy_squared_sum += (float(obj_pdb.atoms()[f].occ)) * (float(obj_pdb.atoms()[f].occ))
            number_of_atoms = number_of_atoms + 1

        return selected_atoms_occupancy, selected_atoms_occupancy_squared_sum, number_of_atoms
        # print( selected_atoms_occupancy, selected_atoms_occupancy_squared_sum, number_of_atoms)

    ### else, return a matrix of values for each of the element type present in the PDB file
    else:
        selected_atoms = obj_pdb.atom_selection_cache().iselection("not ("
                                                                   "element H or "
                                                                   "element O or "
                                                                   "element C or "
                                                                   "element N) ")

        ### working here ###
        for f in selected_atoms:
            element_array.append(obj_pdb.atoms()[f].element.replace(" ", ""))

        element_array_np = np.unique(np.array(element_array))
        # print(element_array_np)
        for element_type in element_array_np:
            # print ("element_type is", element_type)
            selected_atoms_specific = obj_pdb.atom_selection_cache().iselection("element " + str(element_type) + " ")
            number_of_atoms = 0
            selected_atoms_occupancy = selected_atoms_occupancy_squared_sum = 0
            for f in selected_atoms_specific:
                # print(float(obj_pdb.atoms()[f].occ))
                # the following line calculates the sum of squared occupancy
                selected_atoms_occupancy += (float(obj_pdb.atoms()[f].occ))
                selected_atoms_occupancy_squared_sum += (float(obj_pdb.atoms()[f].occ)) * \
                                                        (float(obj_pdb.atoms()[f].occ))
                number_of_atoms = number_of_atoms + 1
            element_array_np_results = np.array([selected_atoms_occupancy,
                                                 selected_atoms_occupancy_squared_sum,
                                                 number_of_atoms,
                                                 element_type])
            ### appending results into a numpy dataframe
            if first:
                element_array_np_results_summary = element_array_np_results
                first = False
            else:
                element_array_np_results_summary = np.vstack((element_array_np_results_summary,
                                                              element_array_np_results))

        # print("XXXXXXXXXXXXX \n")
        return element_array_np_results_summary


def occupancy_audit_version2(final_refined_model, substructure_PDB_textfile, substructure_atom_type, distance_radius,
                             output_file_name=False, output_occupancy_list=False):
    """
    Compare input substructure_PDB_textfile against final_refined_model and identifies atoms common between the two.
     For each of the atoms (other than H, C, N, O), total occupancy, sum of squared occupancy and number of sites
     in the given substructure_PDB_testfile is calculated.  Most importantly, it uses emma to identify symmetry related
     atoms which takes into account origin shifts and equivalent atoms in different ASUs.

    If output_file_name is provided, then a PDB file of all the common atoms named according to their respective
     atom type in final_refined_model is written out.

    :param output_occupancy_list: to return a numpy matrix of [x,y,z,occ,element_type] array
    :param substructure_PDB_textfile: Input substructure file in PDB file format
    :param final_refined_model: final refined model against which the substructure atoms will be compared with
    :param substructure_atom_type: type of atom for which occupancy needs to be calculated
    :param distance_radius: the distance within which two atoms each from substructure and refined model will be
     considered equivalent
    :param output_file_name: output file name without the extension ".pdb"
    :return: both selected_atoms_occupancy and not_selected_atoms_occupancy and also return the RMSD of the substrucure as
     calculated by phenix.emma
    """
    element_type_array = []
    annotated_substructure_array = []
    all_element_substructure_noise = None
    emma_array = None
    first = True
    first_1 = True

    ## writing out select atoms from the refined model
    pdb_in_refined_model = hierarchy.input(file_name=final_refined_model)
    crystal_symmetry_obj = pdb_in_refined_model.crystal_symmetry()
    selected_atoms_refined_model = pdb_in_refined_model.hierarchy.atom_selection_cache().iselection("not ("
                                                                                                    "element H or "
                                                                                                    "element O or "
                                                                                                    "element C or "
                                                                                                    "element N) ")
    selected_atoms_pdb_hierarchy_refined_model = pdb_in_refined_model.hierarchy.select(selected_atoms_refined_model)
    selected_atoms_pdb_hierarchy_refined_model.write_pdb_file("refined_model_select_atoms.pdb",
                                                              crystal_symmetry=crystal_symmetry_obj)

    ## running emma and grepping the log file ##
    emma_rotation, emma_rmsd, emma_array = runEmma_and_return_equivalent_atom_numbers("refined_model_select_atoms.pdb", substructure_PDB_textfile,
                                                            distance_radius)

    if emma_array is not None:
        pdb_in_reopened_refined_model = hierarchy.input(file_name="refined_model_select_atoms.pdb")

        # delete 'refined_model_select_atoms.pdb' file
        os.remove("refined_model_select_atoms.pdb")

        # print ("emma_array is ", emma_array)
        # if emma_array is not None

        if (emma_array is not None) and (len(emma_array)) > 0:
            ### according to the atom ID, pick the relevant element type from the refined_model_select_atoms pdb file
            # reopening the previous written out "refined_model_selected_atoms.pdb" file
            refined_model_atoms_elements_all = np.array(
                pdb_in_reopened_refined_model.hierarchy.atoms().extract_element())
            refined_model_atoms_elements_select_atoms = []
            for f in emma_array:
                refined_model_atoms_elements_select_atoms.append(
                    (refined_model_atoms_elements_all[int(f[0]) - 1]).strip())

            refined_model_atoms_elements_select_atoms_np = np.array([refined_model_atoms_elements_select_atoms])
            # print("refined_model_atoms_elements_select_atoms_np is \n", refined_model_atoms_elements_select_atoms_np[0][0])

            ##starting from emma_array, pick atoms from substructure_PDB_textfile and assign fine_refined_model element type
            pdb_in = hierarchy.input(file_name=substructure_PDB_textfile)
            refined_model_xyz_np = np.array(pdb_in.hierarchy.atoms().extract_xyz())
            refined_model_occ_np = np.array([pdb_in.hierarchy.atoms().extract_occ()])
            refined_model_selected_atoms_np = np.concatenate((refined_model_xyz_np, refined_model_occ_np.transpose()),
                                                             axis=1)

            # print("refined_model_selected_atoms_np is \n", refined_model_selected_atoms_np)

            if emma_array.ndim == 1:  # check the dimension of the array.  If 1D (not matrix) then proceed
                all_element_substructure_common_atoms = refined_model_selected_atoms_np
            else:
                for f in emma_array:
                    # print("jinga la la ", f)
                    if first:
                        all_element_substructure_common_atoms = np.array(refined_model_selected_atoms_np[int(f[1]) - 1])
                        first = False
                        # print ("all_element_substructure_common_atoms first is ", all_element_substructure_common_atoms)
                    else:
                        all_element_substructure_common_atoms = np.vstack((all_element_substructure_common_atoms,
                                                                           refined_model_selected_atoms_np[
                                                                               int(f[1]) - 1]))

            ### picking all atoms which do not match emma_array list (basically identifying noisy peaks)
            if emma_array.ndim > 1:  # check the dimension of the array.  If 1D (not matrix) then proceed
                first = True
                for f in range(refined_model_selected_atoms_np.shape[0]):
                    if not np.isin(f + 1, list(emma_array[:, -1].astype(int))):
                        # print ("noisy peaks", f)
                        if first:
                            all_element_substructure_noise = np.array(refined_model_selected_atoms_np[f])
                            first = False
                            # print ("all_element_substructure_noise first is ", all_element_substructure_noise)
                        else:
                            all_element_substructure_noise = np.vstack((all_element_substructure_noise,
                                                                        refined_model_selected_atoms_np[f]))
                            # print("all_element_substructure_noise second is ", all_element_substructure_noise)
            else:
                # print(emma_array[-1].astype(int))
                # print ("before delete ", refined_model_selected_atoms_np)
                all_element_substructure_noise = np.delete(refined_model_selected_atoms_np,
                                                           (emma_array[-1].astype(int) - 1),
                                                           0)  # delete the entry matching from emma
                # print ("after delete ", refined_model_selected_atoms_np)

            # print ("all_element_substructure_noise is \n ",  all_element_substructure_noise)
            # print ("\n XXXXXX XXXXXX XXXXXX \n")

            uniq_noise_np = []

            uniq_noise_np.append("NN")  ## adding noise atom_type type peak

            if all_element_substructure_noise is None:  ## if there are not noisy peaks at all
                uniq_noise_np.append(0)
                uniq_noise_np.append(0)
                uniq_noise_np.append(0)
            else:
                if all_element_substructure_noise.ndim > 1:  # if more than one noise peaks
                    uniq_noise_np.append((len(all_element_substructure_noise)))  # number of sites
                    # print("jinga lala", all_element_substructure_noise)
                    uniq_noise_np.append(
                        np.round((np.sum(np.asfarray(all_element_substructure_noise[:, 3], float))),
                                 3))  # sum of occupancies
                    uniq_noise_np.append(np.round((np.sum(
                        np.asfarray(all_element_substructure_noise[:, 3], float) *
                        np.asfarray(all_element_substructure_noise[:, 3], float)
                    )), 3))  # sum of squared occupancies
                elif all_element_substructure_noise.ndim == 1:  # if there is single noisy peak
                    uniq_noise_np.append(1)  # number of sites
                    uniq_noise_np.append(np.round((all_element_substructure_noise[-1]), 3))  # sum of occupancies
                    uniq_noise_np.append(np.round
                                         ((all_element_substructure_noise[-1] *
                                           all_element_substructure_noise[-1]), 3))  # sum of squared occupancies

            # print("all_element_substructure_common_atoms second is ", all_element_substructure_common_atoms)
            ## attaching element type
            # print("before this is all_element_substructure_common_atoms \n", all_element_substructure_common_atoms)
            # print("refined_model_atoms_elements_select_atoms_np is \n",
            #       refined_model_atoms_elements_select_atoms_np.transpose())

            # temp_array = np.array(emma_array.transpose()[:, 1])
            # print("jinga \n", emma_array.transpose()[1:].transpose() )

            # attaching element type
            if emma_array.ndim > 1:  # check the dimension of the array.  If 1D (not matrix) then proceed
                all_element_substructure_common_atoms = np.concatenate((all_element_substructure_common_atoms,
                                                                        refined_model_atoms_elements_select_atoms_np.transpose()),
                                                                       axis=1)

            # print ("all_element_substructure_common_atoms is \n", all_element_substructure_common_atoms)

            # print(" this is all_element_substructure_common_atoms \n", all_element_substructure_common_atoms)

            # write out the renamed PDB file according to respective atom types only if output_file_name is provided.
            if output_file_name:
                ### write out the AX_renamed_PDB file
                PDB_from_numpy_array(crystal_symmetry_obj, all_element_substructure_common_atoms, output_file_name)
            if output_occupancy_list:

                if emma_array.ndim == 1:  # check the dimension of the array.  If 1D (not matrix) then proceed
                    temp_array = []
                    # print ("all_element_substructure_common_atoms[-1] is ", all_element_substructure_common_atoms[0][-1])

                    atom_type = refined_model_atoms_elements_select_atoms_np[0][0]
                    number_of_sites = 1
                    occupancy = all_element_substructure_common_atoms[0][-1].astype(float)

                    # print(atom_type, number_of_sites, occupancy)
                    temp_array.append(atom_type)
                    temp_array.append(number_of_sites)
                    temp_array.append(occupancy)
                    temp_array.append(np.round((occupancy * occupancy), 3))

                    # print ("temp_array ", temp_array)

                    annotated_substructure_array = np.array(temp_array)

                    # annotated_substructure_array = np.array([atom_type,
                    #                                          number_of_sites,
                    #                                          # number os site, which is one if the ndim is array and not matrix
                    #                                          occupancy,  # occupancy
                    #                                          np.round((occupancy * occupancy), 3)])
                else:
                    first = True
                    uniq_element_np = np.unique(all_element_substructure_common_atoms[:, -1])
                    # print (uniq_element_np)
                    for f in uniq_element_np:
                        select_element_substructure_common_atoms = all_element_substructure_common_atoms[
                            all_element_substructure_common_atoms[:, -1] == f]
                        # print (select_element_substructure_common_atoms)
                        # print ("XXXXXXXXXXXXX " + str(f) + " XXXXXXXXXX")
                        # print (np.asfarray(select_element_substructure_common_atoms[:, 3], float))
                        element_type_array.append(f)  # element type
                        element_type_array.append(len(select_element_substructure_common_atoms))  # number of sites
                        element_type_array.append(
                            np.round((np.sum(np.asfarray(select_element_substructure_common_atoms[:, 3], float))), 3)
                        )  # sum of occupancies
                        element_type_array.append(
                            np.round((np.sum(np.asfarray(select_element_substructure_common_atoms[:, 3], float) *
                                             np.asfarray(select_element_substructure_common_atoms[:, 3], float)
                                             )), 3))  # sum of sqared occupancies
                        if first:
                            annotated_substructure_array = np.array(element_type_array)
                            first = False
                            element_type_array = []
                            # print ("annotated_substructure_array first", annotated_substructure_array)
                        else:
                            annotated_substructure_array = np.vstack((annotated_substructure_array,
                                                                      (np.array(element_type_array)).transpose()))
                            # print("annotated_substructure_array 2", annotated_substructure_array)
                            element_type_array = []

                # adding information regarding noise peaks
                annotated_substructure_array = np.vstack((annotated_substructure_array,
                                                          (np.array(uniq_noise_np)).transpose()
                                                          ))

                return emma_rotation, emma_rmsd, annotated_substructure_array


def occupancy_audit(substructure_PDB_textfile, final_refined_model, substructure_atom_type, distance_radius,
                    output_file_name=False, output_occupancy_list=False):
    """
    Compare input substructure_PDB_textfile against final_refined_model and identifies atoms common between the two.
     For each of the atoms (other than H, C, N, O), total occupancy, sum of squared occupancy and number of sites
     in the given substructure_PDB_testfile is calculated.

    If output_file_name is provided, then a PDB file of all the common atoms named according to their respective
     atom type in final_refined_model is written out.

    :param output_occupancy_list: to return a numpy matrix of [x,y,z,occ,element_type] array
    :param substructure_PDB_textfile: Input substructure file in PDB file format
    :param final_refined_model: final refined model against which the substructure atoms will be compared with
    :param substructure_atom_type: type of atom for which occupancy needs to be calculated
    :param distance_radius: the distance within which two atoms each from substructure and refined model will be
     considered equivalent
    :param output_file_name: output file name without the extension ".pdb"
    :return: both selected_atoms_occupancy and not_selected_atoms_occupancy
    """
    selected_atoms_occupancy = not_selected_atoms_occupancy = 0
    refined_selected_atoms_occupancy = refined_not_selected_atoms_occupancy = 0
    element_type_array = []
    annotated_substructure_array = []
    first = True
    first_1 = True

    ###expanding to P1
    expand_to_p1(substructure_PDB_textfile, "p1_expanded")
    ### processing atoms of substructure
    pdb_in = hierarchy.input("p1_expanded.pdb")
    crystal_symmetry_obj = pdb_in.crystal_symmetry()
    obj_pdb = pdb_in.construct_hierarchy()

    # for selecting atom_type of only primary anomalous scatterer
    # selected_atoms = obj_pdb.atom_selection_cache().iselection("element " + str(substructure_atom_type) + " ")

    # for selecting every type of atom within the substructure
    selected_atoms = obj_pdb.atom_selection_cache().iselection("not ("
                                                               "element H or element O or element C or "
                                                               "element N) ")
    selected_atoms_pdb_hierarchy = pdb_in.hierarchy.select(selected_atoms)
    substructure_xyz_np = np.array(selected_atoms_pdb_hierarchy.atoms().extract_xyz())
    substructure_occ_np = np.array([selected_atoms_pdb_hierarchy.atoms().extract_occ()])
    # print (substructure_xyz_np)
    # print (substructure_occ_np)
    # print(substructure_xyz_np.shape)
    # print(substructure_occ_np.shape)
    # print (substructure_occ_np.transpose().shape)
    substructure_selected_atoms_np = np.concatenate((substructure_xyz_np, substructure_occ_np.transpose()), axis=1)

    # print("substructure_selected_atoms_np shape is", substructure_selected_atoms_np.shape)

    # not_selected_atoms = obj_pdb.atom_selection_cache().iselection("not element " + str(atomType) + " ")

    ###processing of refined models expanded to P1 symmetry
    # expand_to_p1(final_refined_model, "p1_expanded")

    # ## extracting all atoms
    # pdb_in = hierarchy.input(file_name=final_refined_model)
    # refined_model_xyz_np = np.array(pdb_in.hierarchy.atoms().extract_xyz())
    # refined_model_occ_np = np.array([pdb_in.hierarchy.atoms().extract_occ()])
    # # the following line concatenates xyz coordinates with occupancy column (which is usually peak height)
    # refined_model_selected_atoms_np = np.concatenate((refined_model_xyz_np, refined_model_occ_np.transpose()), axis=1)

    ## extracting all atoms of p1_expanded model
    # pdb_in_model = hierarchy.input(file_name="p1_expanded.pdb")
    # refined_model_xyz_np = np.array(pdb_in_model.hierarchy.atoms().extract_xyz())
    # refined_model_occ_np = np.array([pdb_in_model.hierarchy.atoms().extract_occ()])
    # # the following line concatenates xyz coordinates with occupancy column (which is usually peak height)
    # refined_model_selected_atoms_np = np.concatenate((refined_model_xyz_np, refined_model_occ_np.transpose()), axis=1)

    ##expanding to p1
    expand_to_p1(final_refined_model, "p1_expanded")
    ## extracting elements other than atom_type, C, H, O, N.
    pdb_in = hierarchy.input("p1_expanded.pdb")
    selected_atoms = pdb_in.hierarchy.atom_selection_cache().iselection("not ("
                                                                        "element H or element O or element C or "
                                                                        "element N) ")
    selected_atoms_pdb_hierarchy = pdb_in.hierarchy.select(selected_atoms)
    # selected_atoms_pdb_hierarchy.show()
    ##storing list of all non-protenious and non substructure_atom_type in a an array called interest_elements_np
    interest_elements_np = np.unique(np.array(selected_atoms_pdb_hierarchy.atoms().extract_element()))
    # print("interest_elements_np is ", interest_elements_np)

    if len(interest_elements_np) > 0:
        ## iterating over the list of interest_elements_np and do matching atoms only for each of select atoms in the array
        for specific_element_type in interest_elements_np:
            # print("XXXXXXXXXXXXX printing specific atom type" + specific_element_type + " XXXXXXXXXXXX")
            specific_selected_atoms = pdb_in.hierarchy.atom_selection_cache().iselection(
                "element " + specific_element_type + " ")
            specific_selected_atoms_pdb_hierarchy = pdb_in.hierarchy.select(specific_selected_atoms)
            refined_model_xyz_np = np.array(specific_selected_atoms_pdb_hierarchy.atoms().extract_xyz())
            refined_model_occ_np = np.array([specific_selected_atoms_pdb_hierarchy.atoms().extract_occ()])
            refined_model_selected_atoms_np = np.concatenate((refined_model_xyz_np, refined_model_occ_np.transpose()),
                                                             axis=1)

            specific_element_substructure_common_atoms = identify_common_atoms_within_two_structures(
                substructure_selected_atoms_np,
                refined_model_selected_atoms_np,
                distance_radius)

            # print("specific_element_substructure_common_atoms is ", specific_element_substructure_common_atoms)

            if specific_element_substructure_common_atoms is not None:

                ## constructing annotated substructure:
                temp_array = []
                for f in range(0, len(specific_element_substructure_common_atoms)):
                    temp_array.append([specific_element_type])

                array_of_substructure_atom_type = np.array(temp_array)
                # print (array_of_substructure_atom_type)
                # print (specific_element_substructure_common_atoms)

                specific_element_substructure_common_atoms_plus_atomType = np.concatenate(
                    (specific_element_substructure_common_atoms,
                     array_of_substructure_atom_type),
                    axis=1)

                if first:
                    all_element_substructure_common_atoms = specific_element_substructure_common_atoms_plus_atomType
                    first = False
                else:
                    all_element_substructure_common_atoms = np.vstack((all_element_substructure_common_atoms,
                                                                       specific_element_substructure_common_atoms_plus_atomType))

                element_type_array.append(specific_element_type)  # element name
                element_type_array.append(len(specific_element_substructure_common_atoms))  # number of sites
                element_type_array.append(np.round((
                    np.sum(specific_element_substructure_common_atoms[:, 3])), 3))  # sum of occupancies
                # print ("jingalala ", np.round((np.sum(specific_element_substructure_common_atoms[:, 3] *
                #                                  specific_element_substructure_common_atoms[:,
                #                                  3])),3))
                element_type_array.append(np.round((np.sum(specific_element_substructure_common_atoms[:, 3] *
                                                           specific_element_substructure_common_atoms[:,
                                                           3])), 3))  # sum of sqared occupancies
                # element_type_array.append("occ")

                # print ("element_type_array before", element_type_array)

                if first_1:
                    annotated_substructure_array = np.array(element_type_array)
                    first_1 = False
                    element_type_array = []
                    # print ("annotated_substructure_array first", annotated_substructure_array)
                else:
                    annotated_substructure_array = np.vstack((annotated_substructure_array,
                                                              (np.array(element_type_array)).transpose()))
                    # print("annotated_substructure_array 2", annotated_substructure_array)
                    element_type_array = []

    # write out the renamed PDB file according to respective atom types only if output_file_name is provided.
    if output_file_name:
        PDB_from_numpy_array(crystal_symmetry_obj, all_element_substructure_common_atoms, output_file_name)

    if output_occupancy_list:
        # print("specific_element_substructure_common_atoms_plus_atomType is ", all_element_substructure_common_atoms[:, 3:5])
        return all_element_substructure_common_atoms[:, 3:5]

    else:
        return annotated_substructure_array


def numpy_transform_coordinates(crystal_symmetry_object, numpy_array, transformation):
    """
    Transforms numpy array from orthogonal to fractional or back depending on the input argument.

    :param crystal_symmetry_object: CCTBX crystal_symmetry object
    :param numpy_array: Numpy array which needs to be transformed
    :param transformation: The type of transformation which needs to be applied.
        "orthogonalize" will transform the numpy array from fractional to orthogonal and "fractionalize" will
        do the opposite.
    :return: returns the transformed numpy array
    """
    flex_array = flex.vec3_double()
    transformed_coordinates = flex.vec3_double()
    transformed_coordinates_numpy = []

    for f in numpy_array:
        flex_array.append(f)

    if transformation == "orthogonalize":
        transformed_coordinates = crystal_symmetry_object.unit_cell().orthogonalize(flex_array)
    elif transformation == "fractionalize":
        transformed_coordinates = crystal_symmetry_object.unit_cell().fractionalize(flex_array)

    transformed_coordinates_numpy = np.array(transformed_coordinates)
    return transformed_coordinates_numpy


def extract_numpy_array_from_PDB(PDB_textfile, remove_origin=False, top_n_percent_peaks=None):
    """
    Extracts xyz coordinates and Occupancy column (which also corresponds to peak height values)
     from PDB file and returns it as numpy array

    :param top_n_percent_peaks: Top percentage of peaks to considered (always provided at percentage 0 to 100)
    :param remove_origin: Optionally remove origin for the calculated patterson peaks.
    :param PDB_textfile:input file in PDB file format
    :return: returns xyz coordinates as numpy array
    """
    numpy_array = []
    pdb_in = hierarchy.input(file_name=PDB_textfile)
    atoms_xyz = np.array(pdb_in.hierarchy.atoms().extract_xyz())
    occ_array = np.array([pdb_in.hierarchy.atoms().extract_occ()])
    # the following line concatenates xyz coordinates with occupancy column (which is usually peak height)
    numpy_array = np.concatenate((atoms_xyz, occ_array.transpose()), axis=1)

    if top_n_percent_peaks:
        top_n_peaks = int(len(numpy_array) * (top_n_percent_peaks / 100.0))

        numpy_array = numpy_array[1:top_n_peaks]

    if remove_origin:
        while float(numpy_array[:1, 3]) == float(100.0):
            numpy_array = np.delete(numpy_array, 0, axis=0)  # deletes the first row while peak height is 100
        return numpy_array
    else:
        return numpy_array


def identify_common_atoms_within_two_structures(fixed_sites, moved_sites, tolerance_radius):
    """
    Calculates cross-distance between each position of two input arrays.  A array of constellation of atoms
    (non-redundant) common between the two is returned. This do not take into account origin shifts and comparisions
    with different ASUs.

    :param fixed_sites: Numpy array which will be fixed while searching. Should be in orthogonal coordinates.
    :param moved_sites: Numpy array which will be moved while searching. Should be in orthogonal coordinates.
    :param tolerance_radius: A threshold within which two positions between fixed_sites and moved_sites will be
        considered as equivalent.
    :return: Numpy array of positions common between two numpy arrays
    """
    atom_constellation_array = []
    list_of_constellations_set = set()
    # copying only the coordinates (first 3 columns) of the array; last column is peak height
    fixed_xyz = fixed_sites[:, :3]
    moved_xyz = moved_sites[:, :3]

    # print ("fixed_xyz is", fixed_xyz)
    # print("moved_xyz is", moved_xyz)

    # only the xyz coordinates is used for calculating distance cross matrix
    distance_matrix = distance.cdist(fixed_xyz, moved_xyz, metric='euclidean')
    common_distance_subset_matrix = np.where(distance_matrix < tolerance_radius)

    # positions matching the index provided in common_distance_subset_matrix[0] is appended to subset_atoms array
    subset_atoms = np.array(list(map(lambda x: (fixed_sites[x]), common_distance_subset_matrix[0])))
    # print("subset_atoms is", subset_atoms)
    if len(subset_atoms) > 0:
        # non-redundant of positions from subset_atoms is added to list_of_constellations_set variable
        list_of_constellations_set.add(tuple([tuple(v) for v in subset_atoms]))
        # converting list_of_constellations_set to numpy_array
        for f in list_of_constellations_set:
            temp_array = np.array(f)
            # print("XXXXXXXXXX", temp_array)

        atom_constellation_array = np.round(np.unique(temp_array, axis=0), 3)
        # print("the shape of returning object is", atom_constellation_array)

        if len(np.round(atom_constellation_array, 3)) > 0:
            # print ("from within nested function",   np.round(atom_constellation_array, 3))
            return np.round(atom_constellation_array, 3)
        else:
            print(
                "Opps!  Probably there are not common atoms between the two input arrays for given threshold.  Consider"
                "increasing threshold.  Psst, check if the input array is not empty!")
            return None


def move_coordinates_within_unitcell(crystal_symmetry_object, numpy_array_ortho):
    """
    moves positions of coordinates to within unit cell by converting the input numpy array from orthogonal to
    fractional and return all numpy array of all positions within unitcell in orthgonal coordinates.

    :param crystal_symmetry_object: CCTBX crystal_symmetry object
    :param numpy_array_ortho: coordinates (xyz only) in numpy array
    :return: array of orthogonal coordinates all within unitcell
    """

    numpy_array_frac = numpy_transform_coordinates(crystal_symmetry_object, numpy_array_ortho, "fractionalize")

    origin = np.array((0, 0, 0))
    # moving positions in fractional coordinates if outside outside of unitcell
    numpy_array_frac[numpy_array_frac >= 1.00000000] = numpy_array_frac[numpy_array_frac >= 1.00000000] \
                                                       - np.array(1.00000000)
    numpy_array_frac[numpy_array_frac <= -1.00000000] = numpy_array_frac[numpy_array_frac <= -1.00000000] \
                                                        - np.array(-1.00000000)

    numpy_array_transformed_ortho = numpy_transform_coordinates(crystal_symmetry_object, numpy_array_frac,
                                                                "orthogonalize")

    return numpy_array_transformed_ortho


def PDB_from_numpy_array(crystal_symmetry_obj, numpy_array, output_file_name, fractional=False):
    """
    Generate PDB file from numpy array (either fractional or orthogonal).
    Though input numpy array has coordinates (xyz), occupancy and atomtype; the occupany is not used
    and is reset to 1.0.
    The bfactor is reset to 30.

    :param crystal_symmetry_obj: CCTBX crystal_symmetry object
    :param numpy_array: coordinates + Occupancy + atom type in numpy array
    :param output_file_name: output file name without the extension ".pdb"
    :param fractional: Fractional = True if the input numpy_array is in fractional coordinates.  If not this
        argument is set to False by default.
    :return: Writes out PDB file
    """
    if fractional:
        print("this is fractional")
        flex_frac_array = flex.vec3_double()
        for f in numpy_array:
            flex_frac_array.append(f)
        flex_ortho_array = crystal_symmetry_obj.unit_cell().orthogonalize(flex_frac_array)
        numpy_array = np.array(flex_ortho_array)
        numpy_array = np.unique(np.round(numpy_array, 3), axis=0)

    output = open(output_file_name + ".pdb", 'w')
    unit_cell = (
        str(crystal_symmetry_obj).rsplit(":")[1].replace("(", "").replace(")", "").replace(",", "").rsplit("\n")[0])
    space_group = (str(crystal_symmetry_obj).rsplit(":")[-1].rsplit("(")[0])
    # print(unit_cell)
    # print("XXXXXXXXXXXXXXXXXXXXX")
    # print(space_group)
    # print("XXXXXXXXXXXXXXXXXXXXX")
    output.write(
        "CRYST1" + "%9s" % unit_cell.rsplit()[0] + "%9s" % unit_cell.rsplit()[1] + "%9s" % unit_cell.rsplit()[2])
    output.write("%7.2f" % float(unit_cell.rsplit()[3]) + "%7.2f" % float(unit_cell.rsplit()[4]) + "%7.2f" % float(
        unit_cell.rsplit()[5]))
    output.write("%3s" % str(space_group) + "\n")
    counter = 1
    for f in numpy_array:
        # print(type(f))
        # print ("XXXXXXXXXXXXXX"+str(np.round(f,2)))
        output.write("HETATM  " + "%3d" % counter + "%3s" % f[4] + "%6s" % "SUB" + "%2s" % "A" + "%4s" % counter)
        output.write(
            "%12.3f" % round(float(f[0]), 3) + "%8.3f" % round(float(f[1]), 3) + "%8.3f" % round(float(f[2]), 3))
        output.write("%7s" % "1.00 " + "30.00 " + "%11s" % f[4] + "\n")
        counter += 1
    output.close()


def create_patterson_image(PDB_textfile):
    """
    Creates a patterson image of the input substructure provided as a PDB text file

    :param PDB_textfile:  substructure provided as PDB text input file :return: Writes out patterson in PDB file
    format in both P1 and other patterson symmetry space groups.  It also returns the coordinate image as a numpy
    array in p1 symmetry.
    """
    print("running Kau now")
    first = True
    origin = np.array((0, 0, 0))
    stacked_coordinates_unique_flexarray = []
    stacked_coordinates = []

    stacked_coordinates_unique_unitcell_flexarray = flex.vec3_double()
    pdb_in = hierarchy.input(file_name=PDB_textfile)

    space_group = (str(pdb_in.input.crystal_symmetry()).rsplit(":")[-1].rsplit("(")[0])

    if (space_group.replace(" ", "")) == "P1":
        cs = pdb_in.crystal_symmetry()
    else:
        cs = pdb_in.crystal_symmetry().patterson_symmetry()

    atoms_xyz = pdb_in.hierarchy.atoms().extract_xyz()

    atoms_xyz_np = np.array(atoms_xyz)

    # print(vector)

    for vector in atoms_xyz_np:
        moved_coordinates = atoms_xyz_np - vector
        # print(vector)
        if first:
            stacked_coordinates = moved_coordinates
            first = False
        else:
            stacked_coordinates = np.vstack((stacked_coordinates, moved_coordinates))

    stacked_coordinates_unique = np.unique(np.round(stacked_coordinates, 7), axis=0)

    stacked_coordinates_unique_unitcell = move_coordinates_within_unitcell(
        cs, stacked_coordinates_unique)
    # print("frac unit cell")
    # print(stacked_coordinates_unique_unitcell_frac)
    # image_creating_2D(stacked_coordinates_unique_unitcell, "patterson_image")

    PDB_from_numpy_array(cs, stacked_coordinates_unique_unitcell, "patterson_image")
    expand_to_p1("patterson_image.pdb", "patterson_image_p1")

    # stacked_coordinates_unique_unitcell_p1 = extract_numpy_array_from_PDB("patterson_image_p1.pdb")
    # # print(stacked_coordinates_unique_unitcell_p1)
    # return stacked_coordinates_unique_unitcell_p1


def pick_peaks(map_input):
    # af=any_file(os.path.join("/home/ksh40/work/phassade/dataDump/kau_dir/2EVR/w2/2EVR_w2_patt.ccp4"))
    af = any_file(map_input)
    ccp4_map = af.file_content
    # crystal_symmetry = ccp4_map.crystal_symmetry().cell_equivalent_p1()
    crystal_symmetry = ccp4_map.crystal_symmetry()
    f_obs = miller.structure_factor_box_from_map(map=ccp4_map.data.as_double(), crystal_symmetry=crystal_symmetry,
                                                 include_000=True)
    print("the range of resolution of the input map is ", f_obs.resolution_range())
    resolution_factor = f_obs.resolution_range()[1]  # taking the maximum resolution range directly from the map input
    print("resolution_factor is ", resolution_factor)

    # fft_map=f_obs.fft_map(resolution_factor=resolution_factor, symmetry_flags=maptbx.use_space_group_symmetry,)
    fft_map = f_obs.fft_map(resolution_factor=1 / 3., symmetry_flags=maptbx.use_space_group_symmetry)

    max_peaks = 1000
    # cutoff = (1.0 / resolution_factor)
    # cutoff=(1/resolution_factor)
    cutoff = None
    min_cross_distance = 1.2
    min_distance_sym_equiv = 0.5
    min_cubicle_edge = 5.0

    print("type of the map is", type(fft_map))

    peak_search_parameters = maptbx.peak_search_parameters(
        peak_search_level=3,
        max_peaks=max_peaks,
        peak_cutoff=cutoff,  # map cutoff above which peaks are selected
        interpolate=True,
        min_distance_sym_equiv=min_distance_sym_equiv,
        general_positions_only=True,
        min_cross_distance=min_cross_distance,  # distance between peaks to consider tham separate
        min_cubicle_edge=min_cubicle_edge)
    peaks = fft_map.peak_search(parameters=peak_search_parameters).all(max_clusters=1000)
    sites = peaks.sites()
    # sites=crystal_symmetry.unit_cell().orthogonalize(peaks.sites()).round(3)
    heights = peaks.heights().round(1)

    lines = [
        "ATOM     92  SG  CYS A  10       8.470  28.863  18.423  1.00 22.05           S"]  # just a random line to set up x-ray structure
    from phenix.autosol.get_pdb_inp import get_pdb_inp
    pdb_inp = get_pdb_inp(lines=lines)
    xrs = pdb_inp.xray_structure_simple(crystal_symmetry=crystal_symmetry)

    scatterers = flex.xray_scatterer()

    if len(heights) < 1:
        print("No peaks")

    max_height = heights[0]

    heights_normalized = ((heights.deep_copy() / max_height) * 100).round(2)

    for site, height in zip(sites, heights_normalized):
        scatterers.append(
            xray.scatterer(scattering_type="C",
                           label="C", site=site, u=0.1, occupancy=height)
        )
    xrs = xray.structure(xrs, scatterers=scatterers)
    f = open("peak_list_with_height.pdb", 'w')
    f.write(xrs.as_pdb_file())
    f.close()


def distance_to_origin(numpy_array):
    distance_from_origin = []
    # input_file_name = PDB_textfile
    # # print(PDB[0])
    # pdb_in = hierarchy.input(file_name=input_file_name)
    # crystal_symmetry = pdb_in.crystal_symmetry()
    # unit_cell = (
    #     str(crystal_symmetry).rsplit(":")[1].replace("(", "").replace(")", "").replace(",", "").rsplit("\n")[0])
    # print (unit_cell)
    xyz_coordinates = numpy_array[:, :3]
    for f in xyz_coordinates:
        distance_from_origin.append(distance.euclidean(f, np.array([[0, 0, 0]])))

    # distance_from_origin = np.array(distance_from_origin)
    print(list(np.round(distance_from_origin, 2)))
    # return distance_from_origin.round(2)


def runEmma_and_return_equivalent_atom_numbers(referencePDB, currentPDB, tolerance):
    """
    This function runs emma.
    :param referencePDB: Reference PDB file against which the comparison needs to be made
    :param currentPDB: Searching PDB file
    :param tolerance: by default, it is set to 3.0
    :return: returns a numpy array of atom numbers which are equivalent between referencePDB and currentPDB
    """
    emma_list = []
    # emma_numpy_array = np.array()
    first = True
    first_1 = True

    tolerance_option = "--tolerance=" + str(tolerance)

    with open("emma.log", 'w') as output:
        pdbtools = sub.Popen(
            ['phenix.emma', referencePDB, currentPDB, tolerance_option],
            # shell=True,
            # stdin=echo.stdout,
            stdout=output,
        )
    pdbtools.communicate()

    with open("emma.log", 'r') as output:
        fileContent = output.read().splitlines()
        i = 0
        lineNumber = 0
        for j in fileContent:
            lineNumber = lineNumber + 1
            if ("rms coordinate differences:" in j) and (first == True):
                rmsd = float(j.rsplit()[-1])
                print (fileContent[lineNumber-3])
                rotation_matrix = fileContent[lineNumber - 3].rsplit(":")[-1] #rotation matrix is stored here
                print(rotation_matrix)
                if str("{-1, 0, 0}, {0, -1, 0}, {0, 0, -1}") in rotation_matrix:
                    print ("matrix is ulta!!")
                counter = 1
                first = False
                while "Singles model" not in fileContent[lineNumber + counter]:
                    # print("jinga", fileContent[lineNumber + counter])
                    emma_list.append(
                        fileContent[lineNumber + counter].rsplit(":")[0].rsplit()[
                            0])  # atom number of refined reference model
                    emma_list.append(
                        fileContent[lineNumber + counter].rsplit(":")[3].rsplit()[-1])  # atom number of model
                    # print (emma_list)
                    counter = counter + 1
                    if first_1:

                        summary_results = np.array(emma_list)
                        first_1 = False
                        emma_list = []
                    else:
                        summary_results = np.vstack((summary_results,
                                                     (np.array(emma_list)).transpose()))
                        # print(summary_results)
                        emma_list = []
    try:
        # print("RMSD is:", rmsd)
        # print ("Values are as below: ", summary_results)
        return rotation_matrix, rmsd, summary_results
    except:
        return None


def read_EP_SAD_log(EP_SAD_log_file):
    f_double_prime = f_prime = 0.0
    atom_f_double_f_prime = []
    first_1 = True
    summary_results = []

    ##for reading a single atomtype f"
    # with open(EP_SAD_log_file, 'r') as output:
    #     fileContent = output.read().splitlines()
    #     i = 0
    #     for j in fileContent:
    #         i = i + 1
    #         if "OUTPUT FILES" in j:
    #             # print ("printing the important line", fileContent[i-13])
    #             # f_double_prime = float(fileContent[i - 25].rsplit()[1])# log file of TOM data
    #             # f_prime = float(fileContent[i - 25].rsplit()[2])# log file of TOM data
    #             f_double_prime = float(fileContent[i - 13].rsplit()[1])  # log file of KAUSHIK data
    #             f_double_prime = float(fileContent[i - 13].rsplit()[2])  # log file of KAUSHIK data
    #
    # return f_prime, f_double_prime

    ## for reading a list of atomtypes
    with open(EP_SAD_log_file, 'r') as output:
        fileContent = output.read().splitlines()
        i = 0
        lineNumber = 0
        for j in fileContent:
            lineNumber = lineNumber + 1
            if "    Atom             f\"           (f')" in j:
                i = i + 1
                if i == 4:
                    counter = 0
                    while "Figures of Merit" not in fileContent[lineNumber + counter + 1]:
                        # print (fileContent[lineNumber+counter])
                        atom_f_double_f_prime.append(
                            fileContent[lineNumber + counter].rsplit()[0])  # atom type uploaded
                        atom_f_double_f_prime.append(fileContent[lineNumber + counter].rsplit()[1])  # f_double_prime
                        atom_f_double_f_prime.append(fileContent[lineNumber + counter].rsplit()[2])  # f_prime
                        # atom_f_double_f_prime.append("FDP")
                        counter = counter + 1
                        if first_1:
                            summary_results = np.array(atom_f_double_f_prime)
                            first_1 = False
                            atom_f_double_f_prime = []
                            # print ("annotated_substructure_array first", annotated_substructure_array)
                        else:
                            summary_results = np.vstack((summary_results,
                                                         (np.array(atom_f_double_f_prime)).transpose()))
                            # print("annotated_substructure_array 2", annotated_substructure_array)
                            atom_f_double_f_prime = []

                        # print ("XXXXXXXXXXXXXXX")

    return summary_results


def read_phassade_log(phassade_log_file):
    with open(phassade_log_file, 'r') as output:
        fileContent = output.read().splitlines()
        i = 0
        for j in fileContent:
            i = i + 1
            if "Information Content: SAD Extra Data" in j:
                # print ("XXXXXXXXXXXXXX entering the trial XXXXXXXXXXXXXX")
                # print (fileContent[i+7])
                # print ("XXXXXXXXXXXXXX entering the trial XXXXXXXXXXXXXX")
                # substructure_content_analysis_number=float(fileContent[i-4].rsplit()[6])
                Estimated_total_information_bits = float(fileContent[i + 7].rsplit()[4])
                Estimated_total_information_nats = float(fileContent[i + 7].rsplit()[7])
                Estimated_total_information_in_reflections = float(fileContent[i + 7].rsplit()[10])
                average_bits_per_reflection = float(fileContent[i + 10].rsplit()[2])
            # elif "phasertng tncs_analysis indicated" in j:
            #     tNCS_type = str(j.rsplit()[-1])
            # elif "substructure_content_analysis delta_bfactor" in j:
            #     delta_bfactor = float(j.rsplit()[-1])
            # elif "substructure_content_analysis number" in j:
            #     estimated_atom_number = float(j.rsplit()[-1])
            elif "phasertng anisotropy delta_bfactor" in j:
                # print("entered function", j)
                anisotropy_bfactor = float(j.rsplit()[-1])
            # elif "phasertng data resolution_available" in j:
            #     high_resolution = float(j.rsplit()[-2])
            #     low_resolution = float(j.rsplit()[-1])
            elif "Strongest Anomalous Scatterer" in j:
                f_double_prime = float(j.rsplit()[-1].rsplit("/")[-1].rsplit(")")[0])
            # elif "SIGI(+) is negative" in j:
            #     negativeFound = True
            # elif "SIGI(-) is negative" in j:
            #     negativeFound = True
            # elif "ERROR" in j:
            #     error_type = True
            #     error_type_txt = j
    return anisotropy_bfactor, f_double_prime


def mtz2sca_conversion(mtz_file):
    with open("mtz2sca.log", 'w') as output:
        mtz2sca = sub.Popen(
            ['mtz2sca', mtz_file],
            # shell=True,
            # stdin=echo.stdout,
            stdout=output,
        )
    mtz2sca.communicate()

def read_shelx_log(shelx_log):
    with open(shelx_log, 'r') as output:
        fileContent = output.read().splitlines()
        for j in fileContent:
            cc_weak = float(j.rsplit(" / ")[-1].rsplit()[0].replace(",", "").strip())
            CC = float(j.rsplit(" / ")[0].rsplit()[-1].strip())
            print (cc_weak, CC)

def eVRMS_Xray(phasingModelPDBID_chainLength, gonnetScore, Molprobity, TargetResolution):
    '''
    This function calculates Hatti function, ePrimeVRMS for Xray models
    '''
    Ak = 0.001455
    Bk = 1.710
    Ck = -0.2444
    Dk = 0.1040
    Ek = 0.01586

    eVRMS = ((Ak*(phasingModelPDBID_chainLength)) +
             (Bk * (math.exp(Ck * (gonnetScore)**(2.5)))) +
             (Dk * Molprobity) +
             (Ek * (TargetResolution)**3)) ** (1 / 2)

    return eVRMS


def test_if_shelx_has_run_100kTrials(shelx_log_file):
    trial_number = 0
    file = open(shelx_log_file, 'r')
    lines = file.readlines()
    trial_number = lines[-4].rsplit()[1].rsplit(',')[0]
    print (trial_number)
    file.close()


###
if (__name__ == "__main__"):
# #     eVRMS_Xray(phasingModelPDBID_chainLength=102, gonnetScore=3.93565217391304, Molprobity=2.7, TargetResolution=2.501)
#
#     # shelx_log_file = os.path.join("/home/ksh40/work/phassade/EP_class/functions/trial_ShelxLog.txt")
#     # read_shelx_log(shelx_log_file)
#     runEmma_and_return_equivalent_atom_numbers('substructure.pdb',
#                                                'PDBCUR.pdb',
#                                                3.0)
    test_if_shelx_has_run_100kTrials("/home/ksh40/work/phassade/EP_class/jinga.log")