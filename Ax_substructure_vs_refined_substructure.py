from __future__ import division, print_function
import time, os, sys, glob, numpy as np
from itertools import count
import subprocess as sub
from shutil import copyfile
import data_generation_class.class_definitions as d_gen_class
from iotbx.pdb import hierarchy
import functions.functions_library as func_lib
import reflection_data_manipulation.reflection_data_manipulation_functions as data_manipulation
import shutil

if (__name__ == "__main__"):

    # Substructure.change_atom_type("/home/ksh40/work/phassade/workingDir/2AVN_w3/11Apr2019/MRSAD/2AVN_w3_AXsubstructure.1.pdb","SE")
    # temp=Phasing_methods("2FG0_w1","/home/ksh40/work/phassade/dataDump/kau_dir/2FG0/2FG0.pdb", "/home/ksh40/temp/phassade/3/jingalalaHo_processed.pdb","/home/ksh40/work/phassade/dataDump/kau_dir/2FG0/w1/2FG0_w1.mtz","/home/ksh40/work/phassade/dataDump/kau_dir/2FG0/2FG0.fa","Se",1.0)
    # temp.runEP_Auto()
    # selected_atoms_occupancy,not_selected_atoms_occupancy=Substructure.calculate_total_occupancy("/home/ksh40/work/phassade/workingDir/6RIM_w1/11Nov2019_SCA/EP_SAD/6RIM_w1.1.pdb", "SE")
    # print(selected_atoms_occupancy,not_selected_atoms_occupancy)
    #
    placeHolder = sys.argv[1]
    # print (placeHolder)
    targetPDBID = placeHolder.rsplit(",")[0].rsplit("_")[0]
    diffraction_ID = placeHolder.rsplit(",")[0].rsplit("_")[1]
    wavelength = float(placeHolder.rsplit(",")[1])
    substructure_atom_type = placeHolder.rsplit(",")[2].rsplit("/")[0]
    # substructure_atom_type = "AX"
    # f_prime = placeHolder.rsplit(",")[3]
    # f_double_prime = placeHolder.rsplit(",")[4]

    # print (targetPDBID,diffraction_ID,wavelength,substructure_atom_type)

    # summary=[]
    # summary.append(targetPDBID)
    # summary.append(diffraction_ID)
    # summary.append(round(wavelength,4))
    # summary.append(knownSites)
    # summary.append(substructure_atom_type)
    # print (summary)
    #
    sourcePath = os.path.join("/mnt/apollo/homes/ksh40/phassade/inputData/kau_dir", targetPDBID)
    if os.path.exists(sourcePath):
        os.chdir(sourcePath)
        # print ("before running")
        # # Substructure.generate_structure_factors("perfect_ha.pdb")
        # # Substructure.generate_patterson_map("perfect_ha.pdb.mtz")
        # print ("Runs completed")

        # destinationPath = os.path.join("/home/ksh40/work/phassade/workingDir")
        destinationPath = os.path.join("/mnt/apollo/homes/ksh40/phassade/work/workingDir")
        runDetails = "positive_control"

        if not os.path.exists(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID)):
            os.mkdir(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID))
            os.chdir(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID))
        else:
            os.chdir(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID))

        if not os.path.exists(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID, runDetails)):
            os.mkdir(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID, runDetails))
            os.chdir(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID, runDetails))
        else:
            os.chdir(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID, runDetails))
        #
        # print ("XXXXXXXXXXX 3 XXXXXXXXXXXX")

        ###creating dir to run MR_SAD ###
        if not os.path.exists(
                os.path.join(destinationPath, str(targetPDBID) + "_" + str(diffraction_ID), runDetails, "MR_SAD")):
            os.mkdir(os.path.join(destinationPath, str(targetPDBID) + "_" + str(diffraction_ID), runDetails, "MR_SAD"))
            os.chdir(os.path.join(destinationPath, str(targetPDBID) + "_" + str(diffraction_ID), runDetails, "MR_SAD"))
        else:
            os.chdir(os.path.join(destinationPath, str(targetPDBID) + "_" + str(diffraction_ID), runDetails, "MR_SAD"))

        ### this is to run "new" datasets which do not have perfect_ha substructure ### ###
        # if os.path.exists(
        #         os.path.join(sourcePath, diffraction_ID, targetPDBID + "_" + diffraction_ID + "_french_wilson.mtz")):
        #     object_holder = d_gen_class.Phasing_methods(targetPDBID + "_" + diffraction_ID,
        #                                                 os.path.join(sourcePath, targetPDBID + ".pdb"),
        #                                                 os.path.join(sourcePath, targetPDBID + ".pdb"),
        #                                                 os.path.join(sourcePath, diffraction_ID,
        #                                                              targetPDBID + "_" + diffraction_ID + "_french_wilson.mtz"),
        #                                                 os.path.join(sourcePath, targetPDBID + ".fa"),
        #                                                 substructure_atom_type, wavelength)
        #
        #     object_holder.runMRsad()  # running MRSAD
        #     print ( "redirectedOutput_withFW", targetPDBID + "_" + diffraction_ID)
        # else:
        #     object_holder = d_gen_class.Phasing_methods(targetPDBID + "_" + diffraction_ID,
        #                                                 os.path.join(sourcePath, targetPDBID + ".pdb"),
        #                                                 os.path.join(sourcePath, targetPDBID + ".pdb"),
        #                                                 os.path.join(sourcePath, diffraction_ID,
        #                                                              targetPDBID + "_" + diffraction_ID + ".mtz"),
        #                                                 os.path.join(sourcePath, targetPDBID + ".fa"),
        #                                                 substructure_atom_type, wavelength)
        #     object_holder.runMRsad()  # running MRSAD
        #     print("redirectedOutput_withoutFW", targetPDBID + "_" + diffraction_ID)

        #writing out PDB with equivalent atomtype as in refined model starting with AX_atoms
        AX_PDBID = os.path.join(targetPDBID + "_" + diffraction_ID + "_AXsubstructure.1.pdb")
        refined_model = os.path.join(sourcePath, targetPDBID + ".pdb")
        try:
            if os.path.exists(AX_PDBID):
                selected_atoms_occupancy, selected_atoms_occupancy_squared_sum, number_of_atoms = \
                    func_lib.calculate_total_occupancy(AX_PDBID, "AX")
                if number_of_atoms>0:
                    # run to do occupany audit and to run emma comparing AX pdb with refined PDB.
                    anomalous_scatterers_list = func_lib.occupancy_audit_version2(refined_model,
                                                                                  AX_PDBID,
                                                                                  substructure_atom_type,
                                                                                  3.0,
                                                                                  output_file_name = targetPDBID + "_" + diffraction_ID +
                                                                                  "_perfect_ha_subset_as_in_refined_model",
                                                                                  output_occupancy_list=False)
                    MR_SAD_file_perfect_ha_selectedAtomOnly = os.path.join(destinationPath, str(targetPDBID) + "_" + str(diffraction_ID),
                                                                           runDetails,
                                                                           "MR_SAD",
                                                                           str(targetPDBID) + "_" + str(diffraction_ID) +
                                                                           "_perfect_ha_subset_as_in_refined_model.pdb"
                                                                           )
                    print ("MR_SAD_file_perfect_ha_selectedAtomOnly path is ", MR_SAD_file_perfect_ha_selectedAtomOnly)
                    print("redirectedOutput_successful", targetPDBID + "_" + diffraction_ID)
                else:
                    print("noAXsubstructure_redirectedOutput_noAnoSignal_redirectedOutput",
                          targetPDBID + "_" + diffraction_ID)
            else:
                print("noAXsubstructure_redirectedOutput_noAnoSignal_redirectedOutput", targetPDBID + "_" + diffraction_ID)
        except:
            print("failed_redirectedOutput", targetPDBID + "_" + diffraction_ID)


        # # #creating dir to run EP_SAD  ###
        if not os.path.exists(os.path.join(destinationPath, str(targetPDBID) + "_" + str(diffraction_ID), runDetails,
                                           "EP_SAD_withPerfectHa_fromRefinedModel_withoutLLGcompletion_fullResolution")):
            os.mkdir(os.path.join(destinationPath, str(targetPDBID) + "_" + str(diffraction_ID), runDetails,
                                  "EP_SAD_withPerfectHa_fromRefinedModel_withoutLLGcompletion_fullResolution"))
            os.chdir(os.path.join(destinationPath, str(targetPDBID) + "_" + str(diffraction_ID), runDetails,
                                  "EP_SAD_withPerfectHa_fromRefinedModel_withoutLLGcompletion_fullResolution"))
        else:
            os.chdir(os.path.join(destinationPath, str(targetPDBID) + "_" + str(diffraction_ID), runDetails,
                                  "EP_SAD_withPerfectHa_fromRefinedModel_withoutLLGcompletion_fullResolution"))

        if os.path.exists(
                os.path.join(sourcePath, diffraction_ID, targetPDBID + "_" + diffraction_ID + "_french_wilson.mtz")):
            target_mtz = os.path.join(sourcePath, diffraction_ID,
                                      targetPDBID + "_" + diffraction_ID + "_french_wilson.mtz")
        else:
            target_mtz = os.path.join(sourcePath, diffraction_ID, targetPDBID + "_" + diffraction_ID + ".mtz")


        ####  Running EP_AUTO mode
        if not os.path.exists(MR_SAD_file_perfect_ha_selectedAtomOnly):
            print("noAXsubstructure_redirectedOutput_noAnoSignal", placeHolder.rsplit(",")[0],
                  placeHolder.rsplit(",")[2])
        else:
            perfect_ha_selected_atoms_occupancy, perfect_ha_recalculated_total_occ, number_of_atoms = d_gen_class.Substructure.calculate_total_occupancy(
                MR_SAD_file_perfect_ha_selectedAtomOnly, substructure_atom_type)
            print(perfect_ha_selected_atoms_occupancy, perfect_ha_recalculated_total_occ, number_of_atoms)
            if number_of_atoms < 1:
                print("AXsubstructure_withZero_redirectedOutput", placeHolder.rsplit(",")[0],
                      placeHolder.rsplit(",")[2])
            else:
                # object_holder = d_gen_class.Phasing_methods(targetPDBID + "_" + diffraction_ID,
                #                                             os.path.join(sourcePath, targetPDBID + ".pdb"),
                #                                             MR_SAD_file_perfect_ha_selectedAtomOnly,
                #                                             target_mtz,
                #                                             os.path.join(sourcePath, targetPDBID + ".fa"),
                #                                             substructure_atom_type, wavelength)
                #
                # object_holder.runEP_Auto()
                print("run_Complete_redirectedOutput", placeHolder.rsplit(",")[0])


        ##### occupancy auditing and EP_SAD log file
        SAD_PDBID = os.path.join(targetPDBID + "_" + diffraction_ID + ".1.pdb")
        print("the full path of SAD_PDBID is ", SAD_PDBID)
        SAD_log = os.path.join(targetPDBID + "_" + diffraction_ID + ".log")
        SAD_MTZ = os.path.join(str(targetPDBID) + "_" + str(diffraction_ID) + ".1.mtz")
        refined_model = os.path.join(sourcePath, targetPDBID + ".pdb")


        ##### The following was copied from shelx_plus_RefinementRuns.py file
        ### running from within the refineDefault_LLGC
        os.chdir(os.path.join(destinationPath, str(targetPDBID) + "_" + str(diffraction_ID), runDetails,
                              "EP_SAD_withPerfectHa_fromRefinedModel_withoutLLGcompletion_fullResolution"))
        if os.path.exists(SAD_PDBID):
            # # # ###Using pre-refined model, calculate occupancy auditing after comparison using emma
            ### running from previous dir
            emma_rotation_preRefined, emma_rmsd_preRefined, anomalous_scatterers_list_preRefined = func_lib.occupancy_audit_version2(
                MR_SAD_file_perfect_ha_selectedAtomOnly,
                SAD_PDBID,
                substructure_atom_type,
                3.0,
                output_file_name=False,
                output_occupancy_list=True)

            print("XXXXXXXXXXXXXXXX\n")
            for rowArray in anomalous_scatterers_list_preRefined:
                print("redirectedOutputPreRefined_occAudit", targetPDBID + "_" + diffraction_ID,
                      rowArray[0], rowArray[1], rowArray[2],
                      rowArray[3])


            # # # ### calculate occupancy auditing after comparison using emma
            emma_rotation, emma_rmsd, anomalous_scatterers_list = func_lib.occupancy_audit_version2(
                MR_SAD_file_perfect_ha_selectedAtomOnly,
                SAD_PDBID,
                substructure_atom_type,
                3.0,
                output_file_name=False,
                output_occupancy_list=True)

            ###if rotation matrix is opposite hand
            if str("{-1, 0, 0}, {0, -1, 0}, {0, 0, -1}") in emma_rotation:
                SAD_MTZ = os.path.join(str(targetPDBID) + "_" + str(diffraction_ID) + ".1.hand.mtz")
            else:
                SAD_MTZ = os.path.join(str(targetPDBID) + "_" + str(diffraction_ID) + ".1.mtz")

            ###calculating map-model CC
            # localCC = d_gen_class.Substructure.runCC_MTZ_PDB(SAD_MTZ,
            #                                                  os.path.join(sourcePath, targetPDBID + ".pdb"))

            print("redirectedOutput_localCC", targetPDBID + "_" + diffraction_ID,
                  emma_rmsd_preRefined,
                  emma_rmsd)

            atom_f_double_f_prime_list = np.array(func_lib.read_EP_SAD_log(SAD_log))
            print("atom_f_double_f_prime_list is :", atom_f_double_f_prime_list)

            # print (anomalous_scatterers_list)
            if anomalous_scatterers_list is not None:
                if anomalous_scatterers_list.ndim > 1:
                    for f in anomalous_scatterers_list:
                        if f[0] == atom_f_double_f_prime_list[0]:
                            print("redirectedOutputRefined_occAudit", targetPDBID + "_" + diffraction_ID,
                                  f[0], f[1], f[2], f[3], atom_f_double_f_prime_list[0],
                                  atom_f_double_f_prime_list[1],
                                  atom_f_double_f_prime_list[2])
                        else:
                            print("redirectedOutputRefined_occAudit", targetPDBID + "_" + diffraction_ID,
                                  f[0], f[1], f[2], f[3])
                else:
                    if anomalous_scatterers_list[0] == atom_f_double_f_prime_list[0]:
                        print("redirectedOutputRefined_occAudit",
                              targetPDBID + "_" + diffraction_ID,
                              anomalous_scatterers_list[0],
                              anomalous_scatterers_list[1],
                              anomalous_scatterers_list[2],
                              anomalous_scatterers_list[3],
                              atom_f_double_f_prime_list[0],
                              atom_f_double_f_prime_list[1],
                              atom_f_double_f_prime_list[2]
                              )
                    else:
                        print("redirectedOutputRefined_occAudit",
                              targetPDBID + "_" + diffraction_ID,
                              anomalous_scatterers_list[0],
                              anomalous_scatterers_list[1],
                              anomalous_scatterers_list[2],
                              anomalous_scatterers_list[3])
            else:
                print("failed_redirectedOutput_emmaNoMatch", targetPDBID + "_" + diffraction_ID)

            shutil.rmtree("./temp_dir", ignore_errors=True)

# # print ("SAD_PDBID file is", SAD_PDBID)
        # # print("SAD_PDBID file is", os.getcwd())
        #
        # ## try:
        # # localCC = d_gen_class.Substructure.runCC_MTZ_PDB(SAD_MTZ,
        # #                                                  os.path.join(sourcePath, targetPDBID + ".pdb"))
        #
        # # print ("redirectedOutput_localCC", targetPDBID + "_" + diffraction_ID, localCC)
        #
        # ##final refined LLG
        # #grepping Final LLG score from SAD log file:
        # with open(SAD_log, 'r') as output:
        #     FinalLLG = None
        #     fileContent = output.read().splitlines()
        #     i = 0
        #     lineNumber = 0
        #     for j in fileContent:
        #         lineNumber = lineNumber + 1
        #         if ("Final Log-Likelihood" in j):
        #             FinalLLG = j.rsplit()[-1]
        # print("redirectOutput_LLG", targetPDBID + "_" + diffraction_ID, FinalLLG)
        #
        # # # # ### calculate occupancy auditing
        # anomalous_scatterers_list = np.array(func_lib.calculate_total_occupancy(SAD_PDBID))
        #
        #
        # # print("before editing occupancy audit \n", anomalous_scatterers_list)
        # #
        # ##EP_AUTO log reading as function of scatterer
        # atom_f_double_f_prime_list = np.array(func_lib.read_EP_SAD_log(SAD_log))
        #
        # # print("before editing fDP \n", atom_f_double_f_prime_list)
        #
        # #### if both EPLoG and occupancy audit is turned on ###
        # # ### checking if the anomalous_scatterers_list array is a single dimension array (consisting of only
        # # ### one type of element details)
        # if anomalous_scatterers_list.ndim > 1:
        #     temp_list = []
        #     for f in range(0, anomalous_scatterers_list.shape[0]):
        #         temp_list.append(["ro " + targetPDBID + "_" + diffraction_ID])
        #         # print (temp_list)
        #     PDBID_array = np.array(temp_list)
        #     summary_results = np.concatenate((PDBID_array,
        #                                       atom_f_double_f_prime_list,
        #                                       anomalous_scatterers_list),
        #                                      axis=1)
        # else:
        #     PDBID_array = np.array(["ro " + targetPDBID + "_" + diffraction_ID])
        #     # print("PDBID_array is ", PDBID_array)
        #     summary_results = np.concatenate((PDBID_array,
        #                                       atom_f_double_f_prime_list,
        #                                       anomalous_scatterers_list),
        #                                      axis=None)
        #
        # print(summary_results)
        #
        # # except:
        # #
        # #     print("failed_redirectedOutput", targetPDBID + "_" + diffraction_ID)
