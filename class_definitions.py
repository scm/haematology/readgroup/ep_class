from __future__ import division, print_function
import time, os, sys, glob, numpy as np
import subprocess as sub
import shutil
import ctypes
from phaser import SimpleFileProperties
import phaser
from iotbx import pdb
from iotbx.pdb import hierarchy
from iotbx.file_reader import any_file
from iotbx import crystal_symmetry_from_any
import itertools
from phaser import *
from cctbx import xray
from cctbx import miller
from cctbx.array_family import flex
from iotbx.reflection_file_reader import any_reflection_file
from libtbx.program_template import ProgramTemplate
from cctbx import maptbx
from phenix.substructure.hyss import patterson
from libtbx.utils import Sorry
from phasertng import Phasertng
from phasertng.phil.phil_to_cards import phil_to_cards
from phasertng.phil.master_phil_file import *
from phasertng.phil.converter_registry import *
from phasertng.scripts.phassade import phassade
import pickle

# from neo4j import GraphDatabase
import multiprocessing


# from Bio.PDB import *
# from Bio import SeqIO

class Pipeline_builder:
    '''this class handles the PDB file, MTZ file, substructure file
     and also builds pipelines.

     Attributes:
        model_file: Is the final refined structure of the target in PDB coordinates
        substructure_file: The substructure (as deposited by the authors) in PDB coordinates
        mtz_file: structure factor file (as deposited by the authors)
        sequence_file: Contains the sequence information of the target
        '''

    def __init__(self, model_file, substructure_file, mtz_file, sequence_file, substructure_atom_type):
        '''
        Collects all the inputs for a given PDB and MTZ (specific wavelength) file
        '''
        self.model_file = model_file
        self.substructure_file = substructure_file
        self.mtz_file = mtz_file
        self.sca_file = str(self.mtz_file).replace("mtz", "sca")

        self.sequence_file = sequence_file
        ###MTZ properties
        # self.labels=None
        self.target_resolution = None
        self.mw = None
        self.Ref_label = "I"
        self.Sigma_label = "SIGI"
        self.solvent_content = None
        self.copies_in_ASU = None
        ###Structure of model properties
        # self.phasing_model_length=None

        ###Substructure properties
        self.substructure_atom_type = substructure_atom_type
        self.number_of_atoms_in_substructure = 0
        self.number_of_atoms_in_substructure_averageB = 0
        self.number_of_atoms_in_substructure_totalOcc = 0

        ###protein sequence information
        self.number_of_residues = None
        self.sequence_single_copy = None

        if not os.path.exists(self.model_file):
            print("the following path doesn't exist:", self.model_file)
        if not os.path.exists(self.substructure_file):
            print("the following path doesn't exist:", self.substructure_file)
        if not os.path.exists(self.mtz_file):
            print("the following path doesn't exist:", self.mtz_file)
        if not os.path.exists(self.sequence_file):
            print("the following path doesn't exist:", self.sequence_file)
        else:
            # print("all files could be accessed")
            pass

        ###populating mtz properties ###
        self.MTZobj = any_file(self.mtz_file)
        file_type = self.MTZobj.file_type
        if not file_type == "hkl":
            print("the file is not hkl", self.mtz_file)
        # print (dir(self.MTZobj))
        # print (self.MTZobj.file_description)
        # print (self.MTZobj.file_content)
        # self.miller_arrays=self.MTZobj.file_content.as_miller_arrays()
        # print (self.miller_arrays[1])
        # for f in self.miller_arrays[1]:
        #     print f
        # MTZobj=SimpleFileProperties.GetMtzColumnsTuple(self.mtz_file)
        # self.labels=MTZobj[1]
        # print("XXXXXXXXXXXXXX")
        # print(self.labels)

        ###populating model or protein structure properties ###
        self.model = any_file(self.model_file)
        file_type = self.model.file_type
        if not file_type == "pdb":
            print("The given file is not pdb file", self.model_file)

        ###populating protein sequence properties ###
        self.sequence = any_file(self.sequence_file)
        file_type = self.sequence.file_type
        if not file_type == "seq":
            print("The given file is not sequence file", self.sequence_file)
        self.copies_in_ASU = len(self.sequence.file_content)
        # self.sequence_single_copy=self.sequence.file_content[1].sequence

        ###populating substructure properties ###
        # self.parser=PDBParser()
        # self.substructure=self.parser.get_structure('', substructure_file)
        # for self.atoms_list in self.substructure.get_atoms():
        #     # print(self.atoms_list.get_bfactor())
        #     self.number_of_atoms_in_substructure=self.number_of_atoms_in_substructure+1
        # print (self.number_of_atoms_in_substructure)

        self.substructure = hierarchy.input(self.substructure_file)
        self.substructure_hierarchy = self.substructure.construct_hierarchy()
        self.selected_atoms = self.substructure_hierarchy.atom_selection_cache().iselection(
            "element " + self.substructure_atom_type)
        self.number_of_atoms_in_substructure = len(self.selected_atoms)
        list_of_selected_occ = []
        list_of_selected_b = []
        for e in self.selected_atoms:
            list_of_selected_occ.append((self.substructure_hierarchy.atoms()[e].occ) ** 2)
            list_of_selected_b.append(self.substructure_hierarchy.atoms()[e].b)

        self.number_of_atoms_in_substructure_totalOcc = (np.sum(list_of_selected_occ)) ** (1 / 2)

        # print(list_of_selected_b)
        self.number_of_atoms_in_substructure_averageB = np.mean(list_of_selected_b)

        ###the above part needs to be uncommented####
        # file_type=self.substructure.file_type
        # if not file_type=="pdb":
        #     print ("The given file is not pdb file", self.substructure_file)

        # self.pdb_in=hierarchy.input(self.substructure)
        # print(self.pdb.file_type)
        # print(dir(self.pdb))
        # self.number_of_atoms_in_substructure=

        ###substructure generation###

        # self.expanded_substructure=Substructure.runExpand_to_P1(self.substructure_file)

        pass

    def positive_control_PL(self):
        '''Builds the pipeline for generating positive controls'''
        pass

    def phasing_evaluation_PL(self):
        '''Builds the pipeline for testing structure determination using
        different phasing techniques starting from sub-substructure'''
        pass


class Substructure:
    '''contains all the methods to handle substructure
    Attributes:
        substructure_file: Substructure in PDB file format
        map_file: map data in MTZ file format.  But, not required.
    '''

    def __init__(self, substructure_file, map_file=None):
        self.substructure_file = substructure_file
        self.map_file = map_file
        pass

    def generate_substructure(self):
        '''will sort atoms in substructure based on occupancy, select a combination
        of 1, 2 or 3 (or more, until it makes sense) of atoms and writes to each PDB file
        to be used later as starting point for LLG completion
        input=substructure
        optional_input=B-factor_cutoff, occupancy_cutoff, percentage_of_top_atoms_for_combination
        output=sub_substructure'''

        pass

    def compare_substructure(self):
        '''will compare substructure; can compare either PDB files or PDB to MTZ
        depending on the input arguments'''
        pass

    @staticmethod
    def runExpand_to_P1(PDBfile):
        '''
        This function was provided by Rober Offner.  It expands the structure into
        P1 space group.'''
        pass

    @staticmethod
    def runEmma(substructure_fixed, substructure_moving, outputFileName):
        '''this will compare two substructures with each other accounting for
        any origin shifts and symmetry'''
        # from iotbx import crystal_symmetry_from_any
        # import iotbx.pdb
        # from iotbx.cns import sdb_reader
        # from iotbx.kriber import strudat
        # from iotbx.option_parser import option_parser
        # from cctbx import euclidean_model_matching as emma
        # import sys, os
        # import cctbx.xray
        # from iotbx.command_line import emma
        # emma.run(substructure_fixed,substructure_moving)
        with open(outputFileName, 'w') as output:
            emma = sub.Popen(
                ['phenix.emma', substructure_fixed, substructure_moving],
                # shell=True,
                # stdin=echo.stdout,
                stdout=output,
            )
            emma.communicate()
        rms = 1000
        with open(outputFileName, 'r') as output:
            fileContent = output.read().splitlines()
            counter = 0
            for j in fileContent:
                counter = counter + 1
                if "rotation:" in j:
                    rotation = str(j.rsplit(":")[-1])
                if "translation:" in j:
                    translation = str(j.rsplit(":")[-1])
                if "rms coordinate differences" in j:
                    rms = float(j.rsplit(":")[-1])
                if "Pairs:" in j:
                    substructure_atom_number = int(fileContent[counter].rsplit(":")[0])
        if rms == 1000:
            return (None, None, None, None)
        else:
            return (rms, substructure_atom_number, rotation, translation)

    @staticmethod
    def generate_structure_factors(PDB):
        '''
        this function generates structure factors upto 1.2A from a given pdb
        '''
        print("entered structure factor generation")
        with open("generating_structure_factors.log", 'w') as output:
            fmodel = sub.Popen(
                ['phenix.fmodel', PDB, "high_resolution=1.2"],
                # shell=True,
                # stdin=echo.stdout,
                stdout=output,
            )
            fmodel.communicate()

    @staticmethod
    def generate_patterson_map(MTZ):
        '''
        Generates patterson map (not anamolous!!!) for a given set of reflections
        '''
        print("entered patterson map generation")
        hkl_in = any_reflection_file(file_name=MTZ)
        miller_arrays = hkl_in.as_miller_arrays()
        f_obs = miller_arrays[0]
        # fft_map=f_obs.patterson_map(origin_peak_removal=True, sharpening=True)
        fft_map = f_obs.patterson_map(sharpening=True)
        # print (dir(fft_map))
        fft_map.as_ccp4_map(file_name="patterson.ccp4")

    @staticmethod
    def runCC_MTZ_MTZ(MTZ1, MTZ2):
        '''Will calculate CC between 2 MTZ maps '''

        with open("cc_MTZ_MTZ.log", 'w') as output:
            cc_MTZ_MTZ = sub.Popen(
                ['phenix.get_cc_mtz_mtz', MTZ1, MTZ2],
                stdout=output,
            )
            cc_MTZ_MTZ.communicate()
        # with open("cc_MTZ_MTZ.log",'r') as output:
        #     fileContent=output.read().splitlines()
        #     for j in fileContent:
        #         if "Starting correlation" in j:
        #             localCC=float(j.rsplit(":")[-1])
        #             return (overallCC)

    @staticmethod
    def runCC_MTZ_PDB(MTZ, PDB, globalCC=False):
        '''Will calculate CC between MTZ and the PDB file'''

        with open("cc_MTZ_PDB.log", 'w') as output:
            cc_MTZ_PDB = sub.Popen(
                ['phenix.get_cc_mtz_pdb', MTZ, PDB],
                stdout=output,
            )
            cc_MTZ_PDB.communicate()
        with open("cc_MTZ_PDB.log", 'r') as output:
            fileContent = output.read().splitlines()
            for j in fileContent:
                if globalCC:
                    if "overall CC" in j:
                        globalCC_value = float(j.rsplit(":")[-1])
                        return globalCC_value
                if "local CC" in j:
                    localCC = float(j.rsplit(":")[-1])
                    return localCC

    @staticmethod
    def change_atom_type(PDB, substructure_atom_type):
        '''Will change the atomtype of the AXsubstructure from AX to the
        given atom type of the substructure such as SE or S or I.
        If the occupancy of atom is >1 it will reset it to 1 and leave the
        occupancy untouched if it is less than 1.'''

        outputFileName = str(PDB.rsplit("/")[-1].rsplit(".")[0]) + "_replacedWith_" + substructure_atom_type + ".pdb"
        print(outputFileName)

        ###the following part of the code will reset occupancy of atom###
        ###but we don't need to reset occupancy at this point of time
        ###hence I have commented out the following code ###
        # from iotbx.pdb import hierarchy
        # pdb_in=hierarchy.input(PDB)
        # for chain in pdb_in.hierarchy.only_model().chains():
        #     chain_atoms=chain.atoms()
        #     for atom in chain_atoms:
        #         atom.set_occ(1)
        #         # if atom.occ>0.99:
        #         #     atom.set_occ(1.0)
        #         ## print(atom.occ)
        #         ## atom.set_element("SE")
        #         ## print ("XXXXXXXXXXXXXXXXXXXXX")
        #
        # # print(dir(pdb_in.construct_hierarchy()))
        # f=open(outputFileName,"w")
        # f.write(pdb_in.hierarchy.as_pdb_string(
        #     crystal_symmetry=pdb_in.input.crystal_symmetry()
        # ))
        # f.close()

        with open(PDB) as f:
            newText = f.read().replace("AX", "SE")
        with open(outputFileName, "w") as f:
            f.write(newText)

    @staticmethod
    def calculate_total_occupancy(PDB, atomType):
        selected_atoms_occupancy = recalculated_total_occ = 0

        pdb_in = hierarchy.input(PDB)
        symm = pdb_in.crystal_symmetry()
        obj_pdb = pdb_in.construct_hierarchy()
        selected_atoms = obj_pdb.atom_selection_cache().iselection("element " + str(atomType) + " ")
        # not_selected_atoms=obj_pdb.atom_selection_cache().iselection("not element "+str(atomType)+" ")

        number_of_atoms = 0

        for f in selected_atoms:
            # print(float(obj_pdb.atoms()[f].occ))
            # the following line calculates the sum of squared occupancy
            selected_atoms_occupancy += (float(obj_pdb.atoms()[f].occ))
            recalculated_total_occ += (float(obj_pdb.atoms()[f].occ)) * (float(obj_pdb.atoms()[f].occ))
            number_of_atoms = number_of_atoms + 1
        # print("selected atoms",selected_atoms_occupancy)
        # if len(not_selected_atoms)>0:
        #     for f in not_selected_atoms:
        #         not_selected_atoms_occupancy+=float(obj_pdb.atoms()[f].occ)
        #
        # print("non selected atoms ",not_selected_atoms_occupancy)

        return (selected_atoms_occupancy, recalculated_total_occ, number_of_atoms)


class Phasing_methods(Pipeline_builder):
    '''Contains methods for phasing
    Attributes:
        all attributes are same as for Pipeline_builder class'''

    def __init__(self, PDBID, model_file, substructure_file, mtz_file, sequence_file, substructure_atom_type,
                 wavelength=None):
        '''
        Collects all the inputs for a given PDB and MTZ (specific wavelength) file
        '''
        Pipeline_builder.__init__(self, model_file, substructure_file, mtz_file, sequence_file, substructure_atom_type)
        self.PDBID = PDBID
        # self.model_file=model_file
        # self.substructure_file=substructure_file
        # self.mtz_file=mtz_file
        self.sequence_file = sequence_file
        self.wavelength = wavelength
        # print("wavelength inside Phasing_methods class", self.wavelength)

        self.nproc = 2  # number of CPUs to be used for parallel programming
        # self.substructure_atom_type=substructure_atom_type

        ###populating protein sequence properties ###
        self.sequence = any_file(self.sequence_file)
        file_type = self.sequence.file_type
        if not file_type == "seq":
            print("The given file is not sequence file", self.sequence_file)
        self.copies_in_ASU = len(self.sequence.file_content)
        # self.sequence_single_copy=self.sequence.file_content[1].sequence

    def data_preparation_for_Phaser(self):
        '''The MTZ data is prepared here and the prepared object is returned
        input=MTZ
        optional_input_parameters=list of user_inputs
        output=a python object with prepared information
        '''
        ### more to follow ###
        return self.r

    def runMRsad(self):
        '''
        This function runs MRsad starting from final refined PDB model to generate
        a substructure.  Note: the atom type is "AX" which accounts for imaginary
        contribution of a atom.
        '''
        # self.substructure_atom_type="AX"
        self.PDBID = self.PDBID + "_AXsubstructure"
        print(self.PDBID)
        print(self.model_file)
        print(self.mtz_file)
        print(self.sequence_file)
        print(self.wavelength)
        print(self.substructure_atom_type)

        i = InputEP_DAT()

        waveid = "cuka"
        i.setHKLI(self.mtz_file)
        i.addCRYS_ANOM_LABI(self.PDBID, waveid, "F(+)", "SIGF(+)", "F(-)", "SIGF(-)")
        # i.addCRYS_ANOM_LABI(self.PDBID,waveid,"I","SIGI")
        i.setMUTE(True)
        # o.setPackagePhenix(file_object=redirect_str)
        r = runEP_DAT(i)
        # r = runEP_DAT(i)
        # print (r.logfile())
        if r.Success():
            hkl = r.getMiller()
            Fpos = r.getFpos(self.PDBID, waveid)
            Spos = r.getSIGFpos(self.PDBID, waveid)
            Ppos = r.getPpos(self.PDBID, waveid)
            Fneg = r.getFneg(self.PDBID, waveid)
            Sneg = r.getSIGFneg(self.PDBID, waveid)
            Pneg = r.getPneg(self.PDBID, waveid)
            i = InputEP_AUTO()
            i.setMUTE(True)
            i.setSPAC_HALL(r.getSpaceGroupHall())
            i.setWAVE(self.wavelength)
            i.setCELL6(r.getUnitCell())
            i.setCRYS_MILLER(hkl)
            i.addCRYS_ANOM_DATA(self.PDBID, waveid, Fpos, Spos, Ppos, Fneg, Sneg, Pneg)
            i.setPART_PDB(self.model_file)
            i.setPART_DEVI(0.8)
            i.setPART_VARI("RMS")
            i.setLLGC_COMP(True)
            i.addLLGC_SCAT("AX")  # this is set to default AX from self.substructure_atom_type
            i.addCOMP_PROT_SEQ_NUM(self.sequence_file, 1.)
            i.setTITL(self.PDBID)
            i.setROOT(self.PDBID)
            print("just before running SAD")
            r = runEP_SAD(i)
            print("LogLikelihood = ", r.getLogLikelihood())

            if not r.Success():
                print("Job exit status FAILURE")
                print(r.ErrorName(), "ERROR :", r.ErrorMessage())
                return

            if r.Success():
                print("Phaser has found MR solutions")
                print("Top LLG = %f" % r.getLogLikelihood())
                print("")

            logFile = self.PDBID + ".log"
            f = open(logFile, 'w')
            f.write(r.logfile())
            f.close()

        else:
            print("Job exit status FAILURE")
            print(r.ErrorName(), "ERROR :", r.ErrorMessage())

        # return Substructure(self, mr_sad_pdb, map_data=mr_sad_map)
        pass

    def runEP_Auto(self):
        '''this will perform Phaser LLG completion
        input= MTZ, sub_substructure
        optional_input_parameters=target_resolution, crystal_symmetry, ...
        output= MTZ_output, completed_substructure
        '''
        print(self.PDBID)
        print(self.model_file)
        print(self.mtz_file)
        print(self.sequence_file)
        print(self.wavelength)
        print("from within EP mode", self.substructure_atom_type)

        i = InputEP_DAT()
        waveid = "cuka"

        i.setHKLI(self.mtz_file)
        i.addCRYS_ANOM_LABI(self.PDBID, waveid, "F(+)", "SIGF(+)", "F(-)", "SIGF(-)")
        i.setMUTE(False)
        r = runEP_DAT(i)
        if r.Success():
            hkl = r.getMiller()
            Fpos = r.getFpos(self.PDBID, waveid)
            Spos = r.getSIGFpos(self.PDBID, waveid)
            Ppos = r.getPpos(self.PDBID, waveid)
            Fneg = r.getFneg(self.PDBID, waveid)
            Sneg = r.getSIGFneg(self.PDBID, waveid)
            Pneg = r.getPneg(self.PDBID, waveid)
            i = InputEP_AUTO()
            # filehandler = open("I_object.pickle", 'w')
            # pickle.dump(r, filehandler)
            i.setSPAC_HALL(r.getSpaceGroupHall())
            i.setWAVE(self.wavelength)
            i.setCELL6(r.getUnitCell())
            i.setCRYS_MILLER(hkl)
            i.addCRYS_ANOM_DATA(self.PDBID, waveid, Fpos, Spos, Ppos, Fneg, Sneg, Pneg)
            i.setATOM_PDB(self.PDBID, self.substructure_file)
            i.setATOM_CHAN_SCAT(True)  # This is necessary for "i.setATOM_CHAN_SCAT_TYPE" to work
            i.setATOM_CHAN_SCAT_TYPE(self.substructure_atom_type)  # reset all AX to given substructure_atom_type
            i.setLLGC_COMP(True)  # LLGC is on/off
            # i.setLLGC_SIGM(4.0) # default sigma is 6; it can be changed here
            # i.setLLGC_COMPLETE(True)#deprecated method
            i.addLLGC_SCAT(self.substructure_atom_type)  # substructure_atom_type for LLG completion
            # i.addLLGC_SCAT("ZN") # substructure_atom_type for LLG completion
            # i.addLLGC_SCAT("CL") # substructure_atom_type for LLG completion
            i.addCOMP_PROT_SEQ_NUM(self.sequence_file, 1.)
            # i.addCOMP_PROT_FASTA_NUM(self.sequence_file,1.)#deprecated method
            i.setTITL(self.PDBID)
            i.setROOT(self.PDBID)
            # i.setHAND("OFF") # OFF keyword will turn off performing refinement of opposite hand of the substructure
            # i.addLLGC_CRYS_SCAT_ELEMENT(self.PDBID,self.substructure_atom_type)
            r = runEP_AUTO(i)
            f = open(self.PDBID + '.log', 'w')
            f.write(r.logfile())
            f.close()
            print("LogLikelihood = ", r.getLogLikelihood())
        else:
            print("Job exit status FAILURE")
            print(r.ErrorName(), "ERROR :", r.ErrorMessage())
        return;

    def run_phassade(self, f_prime, f_double_prime, currentWorkingDir):
        # def run_phassade(self):
        '''
        To run phassade from phasertng.
        '''
        # self.substructure_atom_type=substructure_atom_type
        # phasertng.suite.store=logfile
        # phasertng.suite.loggraphs=True

        # print(" XXXXXXXX fluorescence_scan values are ", f_prime, f_double_prime)

        ##### include the following block into phi_str while running with f_prime and f_double_prime ###
        # phasertng.fluorescence_scan{
        #     scatterer='"""+str(self.substructure_atom_type)+"""'
        #     fp='"""+str(f_prime)+"""'
        #     fdp='"""+str(f_double_prime)+"""'
        #     fix_fdp = on}
        #####################################################################################

        self.summary = []
        if str(self.substructure_atom_type).upper() == "SE":
            self.phil_str = """
                            phasertng.hklin.filename ='""" + str(self.mtz_file) + """'
                            phasertng.threads.number=1
                            phasertng.suite.loggraphs = True
                            phasertng.fluorescence_scan{
                                scatterer='""" + str(self.substructure_atom_type) + """'
                                fp='""" + str(f_prime) + """'
                                fdp='""" + str(f_double_prime) + """'
                                fix_fdp = on}
                            phasertng.biological_unit{
                                hetatm
                                  .multiple = True
                                {
                                  element  = '""" + str(self.substructure_atom_type) + """'
                                  number  = 1}
                                  }
                            phasertng.suite.database='""" + str(currentWorkingDir) + """'
                            phasertng.suite.fileroot = '""" + str(currentWorkingDir) + """'
                            # phasertng.suite.store=further
                            phasertng.reflections.wavelength='""" + str(self.wavelength) + """'
                            phasertng.suite.filestem = "phassade"
                            phasertng.biological_unit.sequence.filename ='""" + str(self.sequence_file) + """'
                            phasertng.substructure_content_analysis.scatterer='""" + str(self.substructure_atom_type) + """'
                            phasertng.composition.number_in_asymmetric_unit = 1
                            phasertng.biological_unit.selenomethionine = "on" 
                        """
        else:
            self.phil_str = """
                            phasertng.hklin.filename ='""" + str(self.mtz_file) + """'
                            phasertng.threads.number=1
                            phasertng.suite.loggraphs = True
                            phasertng.fluorescence_scan{
                                scatterer='""" + str(self.substructure_atom_type) + """'
                                fp='""" + str(f_prime) + """'
                                fdp='""" + str(f_double_prime) + """'
                                fix_fdp = on}
                            phasertng.biological_unit{
                                hetatm
                                  .multiple = True
                                {
                                  element  = '""" + str(self.substructure_atom_type) + """'
                                  number  = 1}
                                  }
                            phasertng.suite.database='""" + str(currentWorkingDir) + """'
                            phasertng.suite.fileroot = '""" + str(currentWorkingDir) + """'
                            # phasertng.suite.store=further
                            phasertng.reflections.wavelength='""" + str(self.wavelength) + """'
                            phasertng.suite.filestem = "phassade"
                            phasertng.biological_unit.sequence.filename ='""" + str(self.sequence_file) + """'
                            phasertng.substructure_content_analysis.scatterer='""" + str(self.substructure_atom_type) + """'
                            phasertng.composition.number_in_asymmetric_unit = 1
                        """

        print("phil structure is ", self.phil_str)
        #
        # logfile=open("phassade.log",'w')
        # logfile.write(phassade(user_phil_str=self.phil_str))
        # logfile.close()
        #
        #
        # original=sys.stdout
        # sys.stdout=open("phassade.log","w")
        # phassade(user_phil_str=self.phil_str)
        # sys.stdout=original

        error, phil_str, working_params = phassade(user_phil_str=self.phil_str)
        self.summary.append("redirectedOutput")
        self.summary.append(self.PDBID)
        self.summary.append(self.number_of_atoms_in_substructure)
        self.summary.append(self.number_of_atoms_in_substructure_averageB)
        self.summary.append(self.number_of_atoms_in_substructure_totalOcc)
        self.summary.append("UntilHereCalculated")
        self.summary.append(self.wavelength)
        self.summary.append(working_params.phasertng.substructure_content_analysis.number)
        self.summary.append(working_params.phasertng.substructure_content_analysis.delta_bfactor)
        # print(self.summary)
        ##Danger.  will remove directory and everything within it
        # logFiles=glob.glob("phasertng*")
        # for f in logFiles:
        #     os.remove(f) ###Caution!!!
        ##Danger
        # print ("printing from within phassade",self.summary)
        return ()

    def run_phassade_old(self):
        '''
        To run phassade from old_phaser.  Phassade from old_phaser cannot handle
        Intensity.  Hence, Intensity has been converted to Amplitudes using french_wilson program from cctbx.
        '''
        print("entered the phassade function")
        self.PDBID = self.PDBID + "_phassade"
        print(self.PDBID)
        print(self.model_file)
        print(self.mtz_file)
        print(self.sequence_file)
        print(self.wavelength)
        print(self.substructure_atom_type)
        self.summary = []
        # def runPhassade(mtz,pdbid,seq,atom_type,wave_length,path) :
        i = InputEP_DAT()
        waveid = "cuka"
        i.setHKLI(self.mtz_file)
        i.addCRYS_ANOM_LABI(self.PDBID, waveid, "F(+)", "SIGF(+)", "F(-)", "SIGF(-)")
        i.setMUTE(False)
        r = runEP_DAT(i)
        if r.Success():
            hkl = r.getMiller()
            Fpos = r.getFpos(self.PDBID, waveid)
            Spos = r.getSIGFpos(self.PDBID, waveid)
            Ppos = r.getPpos(self.PDBID, waveid)
            Fneg = r.getFneg(self.PDBID, waveid)
            Sneg = r.getSIGFneg(self.PDBID, waveid)
            Pneg = r.getPneg(self.PDBID, waveid)
            i = InputEP_SSD()
            i.setMUTE(True)
            i.setSPAC_HALL(r.getSpaceGroupHall())
            i.setWAVE(self.wavelength)
            i.setCELL6(r.getUnitCell())
            i.setCRYS_MILLER(hkl)
            i.addCRYS_ANOM_DATA(self.PDBID, waveid, Fpos, Spos, Ppos, Fneg, Sneg, Pneg)
            i.setLLGC_COMP(True)
            i.addLLGC_SCAT(self.substructure_atom_type)
            # i.setFIND_PEAK_CUTO(50) # top 50% (instead of default 75%) peaks selected
            i.addCOMP_PROT_SEQ_NUM(self.sequence_file, 1.)
            i.setTITL(self.PDBID)
            i.setROOT(self.PDBID)
            # i.setTOPF(2)
            r = runEP_SSD(i)
            f = open(self.PDBID + '.log', 'w')
            f.write(r.logfile())
            f.close()
        else:
            print("Job exit status FAILURE")
            print(r.ErrorName(), "ERROR :", r.ErrorMessage())
        return;

    def run_hyss(self, starting_number_of_sites=None):
        """
        Runs hyss followed by LLGC protocol
        :return: returns substructure and hyss log file
        """

        n_sites = "n_sites=" + str(starting_number_of_sites)
        wavelength = "wavelength=" + str(self.wavelength)
        scattering_type = "scattering_type=" + str(self.substructure_atom_type)

        with open("hyss.log", 'w') as output:
            hyss = sub.Popen(
                ['phenix.hyss', self.mtz_file, n_sites, wavelength, scattering_type],
                # shell=True,
                # stdin=echo.stdout,
                stdout=output,
            )
        hyss.communicate()

    def runShelx(self, target_resolution, starting_number_of_sites=None, on_cluster=False):
        '''
        This function runs Shelx C/D/E pipeline starting from structure factors
        '''
        crystal_data = crystal_symmetry_from_any.extract_from(self.mtz_file)
        # solvent_content = str(solvent_content)
        print("target resolution before adding a value", target_resolution)
        resolution = str(float(target_resolution) + 1.5)
        unit_cell = str(crystal_data.unit_cell()).replace(",", "")
        unit_cell = unit_cell.replace("(", "")
        unit_cell = unit_cell.replace(")", "")
        spacegroup = str(crystal_data.space_group_info()).replace(" ", "")
        sca = self.sca_file
        # shelx_sampleScript = os.path.join("/home/ksh40/work/phassade/EP_class/default_scripts/shelxCD_only.sh")
        shelx_sampleScript = os.path.join("/home/ksh40/antikytera_phassade/EP_class/default_scripts/shelxCD_only.sh")

        shutil.copy2(shelx_sampleScript, "shelxCD_only.sh")  # copy the shelx script

        fin = open("shelxCD_only.sh", "rt")
        data = fin.read()
        data = data.replace('number_of_sites', str(starting_number_of_sites))
        data = data.replace('replace_input_data', sca)
        data = data.replace('replace_dimension', unit_cell)
        data = data.replace('replace_spacegroup', spacegroup)
        data = data.replace('replace_resolution', resolution)
        data = data.replace('replace_atomtype', self.substructure_atom_type)
        data = data.replace('replace_pdbid', self.PDBID)
        fin.close()

        fin = open("shelxCD_only.sh", "wt")
        fin.write(data)
        fin.close()

        # copyFunction = 'cp /home/ksh40/work/trial_runs/sampleScripts/shelx_sad.sh ' + self.PDBID + '.sh'
        # os.system(copyFunction)
        # # replace regular expressions
        # sedReplace = 'sed -i \'s/replace_pdbid/' + self.PDBID + '/\' ' + self.PDBID + '.sh'
        # os.system(sedReplace)
        # sedReplaceFa = 'sed -i \'s/replace_pdbid_fa/' + self.PDBID + '_fa/\' ' + self.PDBID + '.sh'
        # os.system(sedReplaceFa)
        # sedReplace = 'sed -i \'s/replace_solvent_content/' + solvent_content + '/\' ' + self.PDBID + '.sh'
        # os.system(sedReplace)
        # sedReplace = 'sed -i \'s/replace_resolution/' + resolution + '/\' ' + self.PDBID + '.sh'
        # os.system(sedReplace)
        # sedReplace = 'sed -i \'s/replace_dimension/' + unit_cell + '/\' ' + self.PDBID + '.sh'
        # os.system(sedReplace)
        # sedReplace = 'sed -i \'s/replace_spacegroup/' + spacegroup + '/\' ' + self.PDBID + '.sh'
        # os.system(sedReplace)
        # sedReplace = 'sed -i \'s/replace_atomtype/' + atom_type + '/\' ' + self.PDBID + '.sh'
        # os.system(sedReplace)
        # sedReplace = 'sed -i \'s/replace_input_data/' + sca + '/\' ' + self.PDBID + '.sh'
        # os.system(sedReplace)

        if on_cluster:
            with open("shelx.log", 'w') as output:
                script = 'shelxCD_only.sh'
                shelx = sub.Popen(
                    ['srun', script],
                    # shell=True,
                    # stdin=echo.stdout,
                    stdout=output,
                )
            shelx.communicate()
        else:
            with open("shelx.log", 'w') as output:
                script = 'shelxCD_only.sh'
                shelx = sub.Popen(
                    ['sh', script],
                    # shell=True,
                    # stdin=echo.stdout,
                    stdout=output,
                )
            shelx.communicate()

    def runRNP(self, phasingModelPDBID_filePath, phasingModelPDBID, eVRMS, SigA_FSOL=None, SigA_BSOL=None,
               BulkSolvent_FSOL=None, BulkSolvent_BSOL=None, six_dimensions=None):
        """
        This function will perform Refinement and phasing
        :return: returns a log file
        """
        if six_dimensions is None:
            six_dimensions = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

        print(self.PDBID)
        print(self.model_file)
        print(self.mtz_file)
        print(self.sequence_file)
        print(self.wavelength)
        print("from within RNP mode")

        ###figuring out labels from the MTZ file
        MTZobj = SimpleFileProperties.GetMtzColumnsTuple(self.mtz_file)
        colLabels = []
        for f in MTZobj[1]:
            if "I" in f.upper():
                colLabels.append(f)
            elif "F" in f.upper() and "FREE" != f.upper():
                colLabels.append(f)
        inputs_I_F = colLabels[0]
        inputs_SIGI_SIGF = colLabels[1]

        ### processing generic data for old phaser run
        i = phaser.InputMR_DAT()
        i.setHKLI(self.mtz_file)
        i.setLABI_F_SIGF(inputs_I_F, inputs_SIGI_SIGF)
        i.setRESO_AUTO_OFF()
        # o = Output()
        # redirect_str = StringIO()
        # o.setPackagePhenix(file_object=redirect_str)
        # r = phaser.runMR_DAT(i, o)
        r = phaser.runMR_DAT(i)
        processed_reflection = r
        if not r.Success():
            print("Job exit status FAILURE")
            print(r.ErrorName(), "ERROR :", r.ErrorMessage())
            sys.exit(1)

        ### performing RNP using the data processed from above
        i = None

        i = phaser.InputMR_RNP()

        nc = 1  # number of copies of the input sequence file

        i.setMUTE(True)  # mute output log
        i.setREFL_DATA(processed_reflection.getREFL_DATA())
        i.setROOT(self.PDBID)
        # i.addENSE_PDB_ID(self.PDBID, pdb, si) # this is to input sequence identity
        i.addENSE_PDB_RMS(self.PDBID, phasingModelPDBID_filePath, eVRMS)  # this is to input RMS directly
        # i.addCOMP_PROT_MW_NUM(mw, nc)
        i.addCOMP_PROT_SEQ_NUM(self.sequence_file, nc)
        if SigA_FSOL or SigA_FSOL >= 0.0:
            i.setSOLP_SIGA_FSOL(float(SigA_FSOL))  # Fsol to test SigmaA
        if SigA_BSOL or SigA_BSOL >= 0.0:
            i.setSOLP_SIGA_BSOL(float(SigA_BSOL))  # Bsol to test SigmaA

        if BulkSolvent_FSOL or BulkSolvent_FSOL >= 0.0:
            i.setSOLP_BULK_USE(True)
            i.setSOLP_BULK_FSOL(float(BulkSolvent_FSOL))# Fsol to test BulkSolvent
        if BulkSolvent_BSOL or BulkSolvent_BSOL >= 0.0:
            i.setSOLP_BULK_BSOL(float(BulkSolvent_BSOL))# Bsol to test BulkSolvent



        # i.addMACM(bool <ROT>,bool <TRA>,bool <BFAC>,bool <VRMS>,bool <CELL>,bool <LAST>, int <NCYC>,str [BFGS"|"NEWTON"|"DESCENT"])
        i.addMACM(True, True, True, True, False, False, True, 50, "BFGS")
        # i.addCOMP_PROT_FASTA_NUM(seq,1.) #Here is where sequence file is defined
        i.addSEAR_ENSE_NUM(self.PDBID, 1)
        i.setSPAC_NAME(processed_reflection.getSpaceGroupName())
        # i.setXYZO(writepdb)
        # i.addSOLU_6DIM_ENSE(self.PDBID,[0.0, 0.0, 0.0],True,[0.0, 0.0, 0.0],sol["bfac"],False,False,False,sol["mult"],sol["ofac"])
        i.addSOLU_6DIM_ENSE(self.PDBID, [0.0, 0.0, 0.0], True, [0.0, 0.0, 0.0], 0.0, False, False, False, 1.0, 1.0)

        r = phaser.runMR_RNP(i)
        if not r.Success():
            print("Job exit status FAILURE")
            print(r.ErrorName(), "ERROR :", r.ErrorMessage())
            return None

        logFile = self.PDBID + "_RNP.log"
        f = open(logFile, 'w')
        f.write(r.logfile())

        rbrr = [sol.KNOWN[-1].getEuler() for sol in r.getDotSol()][0]
        rbrt = [sol.KNOWN[-1].TRA for sol in r.getDotSol()][0]
        rbrllg = [sol.LLG for sol in r.getDotSol()][0]
        rbrzscore = [sol.TFZ for sol in r.getDotSol()][0]
        rbrbfacs = [sol.KNOWN[-1].BFAC for sol in r.getDotSol()][0]
        rbrmults = [sol.KNOWN[-1].MULT for sol in r.getDotSol()][0]
        rbrofacs = [sol.KNOWN[-1].OFAC for sol in r.getDotSol()][0]
        vrms = [z for x in sol.VRMS.values() for z in x][0]
        newvrms = [z for x in sol.NEWVRMS.values() for z in x][0]

        # print "Data at:",inputs["targetPDBID"]
        # print "MR Solution at: ",inputs["phasingModelPDBID"]
        # print "Refined Euler: ",rbrr
        # print "Refined Fracs: ",rbrt
        # print "Refined LLG: ",rbrllg
        # print "Refined Zscore: ",rbrzscore
        # print "Refined VRMSD: ",vrms
        # print "Refined NEW_VRMSD: ",newvrms
        # print "Refined BFactor: ",rbrbfacs
        # print "Refined MULT: ",rbrmults
        # print "Refined OFACS: ",rbrofacs

        # SummaryResults = []
        # SummaryResults.append("redirectedOutput")
        # SummaryResults.append(self.PDBID)
        # SummaryResults.append(phasingModelPDBID)
        # # SummaryResults.append(eVRMS)
        # SummaryResults.append(vrms)
        # SummaryResults.append(newvrms)
        # SummaryResults.append(rbrllg)
        # SummaryResults.append(rbrzscore)
        # SummaryResults.append(rbrbfacs)

        # print(SummaryResults)

        # with open(self.PDBID + ".pickle", "w") as p:
        #     pickle.dump(r, p)

        return np.round(SigA_FSOL, 2), np.round(SigA_BSOL, 2), np.round(BulkSolvent_FSOL, 2), np.round(BulkSolvent_BSOL, 2), newvrms, rbrllg, rbrzscore


class Log_processing:
    '''Will read the log files from various programs and spit out relevant information'''

    def read_Phaser():
        pass

    def read_Autosol():
        pass

    def read_Shelx():
        pass

    def read_CClog():
        pass

    def read_Emmalog():
        pass


class graphDB:
    '''Will access Neo4j graphDB.  Will create nodes/relationship in the graphDB'''

    def __init__(self, uri, user, password):
        '''Will connect to graphDB'''
        self._driver = GraphDatabase.driver(uri, auth=(user, password))
        print("connected to db successfully")

    def doSomething(self):
        with self._driver.session() as session:
            # result=session.run("MATCH (n) RETURN count(n)")
            result = session.run("MATCH()-[n]->() RETURN count(n)")
            # print(dir(result))
        print(result.value())
        result.detach()
        # print(session.read_transaction(result))

    def close(self):
        self._driver.close()

    def create_node(self, name, sex, BornPlace, BornDate):
        '''This function will create node'''
        with self._driver.session() as session:
            return session.run(
                "MERGE (n:Person {SecondName_FirstName:$name, Sex:$sex, BornPlace:$BornPlace, BornDate:$BornDate})"
                "RETURN ID(n)", name=name, sex=sex, BornPlace=BornPlace, BornDate=BornDate).single().value()

    def create_relationship(self, child_name, parent_name):
        '''This function will create relationship between two nodes'''

        with self._driver.session() as session:
            return session.run("MATCH (a:Person{SecondName_FirstName:$a_name}),(b:Person{SecondName_FirstName:$b_name})"
                               "MERGE (a)-[:child_of]->(b)", a_name=child_name, b_name=parent_name)

# if (__name__ == "__main__"):
#     # Substructure.change_atom_type("/home/ksh40/work/phassade/workingDir/2AVN_w3/11Apr2019/MRSAD/2AVN_w3_AXsubstructure.1.pdb","SE")
#     temp = Phasing_methods("2FG0_w2","/home/ksh40/work/phassade/dataDump/kau_dir/2FG0/2FG0.pdb", "/run/media/ksh40/Kaushik/antikythera/work/workingDir/2FG0_w2/positive_control/MR_SAD/2FG0_w2_perfect_ha_subset_as_in_refined_model.pdb","/home/ksh40/work/phassade/dataDump/kau_dir/2FG0/w2/2FG0_w2.mtz","/home/ksh40/work/phassade/dataDump/kau_dir/2FG0/2FG0.fa","Se",1.0)
#     temp.runEP_Auto()
#     temp.run_hyss(starting_number_of_sites=4)
