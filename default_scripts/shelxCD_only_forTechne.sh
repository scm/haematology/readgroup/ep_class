#!/bin/sh -e
#
# Example script running SHELX C/D/E pipeline
# on a SAD experiment using parameters from
# the fastphas.pdf document
source /mnt/apollo/homes/ksh40/software/ccp4-7.0/bin/ccp4.setup-sh
# Run the pipeline
shelxc replace_pdbid <<EOF
SAD replace_input_data
CELL replace_dimension 
SPAG replace_spacegroup 
SHEL 999 replace_resolution
SFAC replace_atomtype
FIND number_of_sites
MIND -1.7
NTRY 2000
EOF
shelxd replace_pdbid_fa
#shelxe replace_pdbid replace_pdbid_fa -h -sreplace_solvent_content -m40 
#shelxe replace_pdbid replace_pdbid_fa -h -sreplace_solvent_content -m40 -i -a
#
