from __future__ import division, print_function
import time, os, sys, glob, numpy as np
import subprocess as sub
from shutil import copyfile
import reflection_data_manipulation.reflection_data_manipulation_functions as data_manipulation

if __name__ == "__main__":
    placeHolder = sys.argv[1]
    # print (placeHolder)
    targetPDBID = placeHolder.rsplit(",")[0].rsplit("_")[0]
    diffraction_ID = placeHolder.rsplit(",")[0].rsplit("_")[1]
    # wavelength = float(placeHolder.rsplit(",")[1])
    # substructure_atom_type = placeHolder.rsplit(",")[2].rsplit("/")[0]

    # print(targetPDBID, diffraction_ID, substructure_atom_type)
    sourcePath = os.path.join("/home/ksh40/work/phassade/dataDump/kau_dir", targetPDBID)

    if os.path.exists(sourcePath):
        # structure_factor_file = os.path.join(sourcePath, targetPDBID+"-sf.cif")
        os.chdir(os.path.join(sourcePath, diffraction_ID))
        print(os.getcwd())
        targetPDBID_MTZ_format = os.path.join(sourcePath, diffraction_ID, targetPDBID+"_" +diffraction_ID+".mtz")
        french_wilsion_log_file = os.path.join(sourcePath, diffraction_ID, "french_wilson.log")

        # diffraction_wavelength, length_a, length_b, length_c, angle_alpha, angle_beta, angle_gamma, spacegroup_name, spacegroup_number = data_manipulation.read_structure_factor(targetPDBID_MTZ_format)
        wavelength = data_manipulation.read_french_wilson_log_file(french_wilsion_log_file)
        label, high_resolution, low_resolution, spacegroup_number, spacegroup_name = data_manipulation.read_data_from_MTZ(targetPDBID_MTZ_format)
        print ("redirectedOutput", targetPDBID+"_" +diffraction_ID, spacegroup_number, spacegroup_name, label, high_resolution, low_resolution, wavelength)
    else:
        print("target_notPresent_redirectedOutput", targetPDBID)
