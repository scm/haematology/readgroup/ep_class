#!/bin/sh
placeHolder=${1}
echo ${placeHolder}
targetPDBID=`echo ${placeHolder} | cut -d, -f1 | cut -d_ -f1 `
diffraction_ID=`echo ${placeHolder} | cut -d, -f1| cut -d_ -f2 `
#wavelength=`echo ${placeHolder} | cut -d, -f2`
#echo ${targetPDBID}
#echo ${diffraction_ID}
#echo ${wavelength}

if [ -d /home/ksh40/work/phassade/dataDump/more_data/melanie_synchrotron/melanie_workingDir/"${targetPDBID}"/"${diffraction_ID}" ]; then
    # shellcheck disable=SC2164
    cd /home/ksh40/work/phassade/dataDump/more_data/melanie_synchrotron/melanie_workingDir/"${targetPDBID}"/"${diffraction_ID}"

#    phenix.french_wilson "${targetPDBID}""_""${diffraction_ID}"".mtz"":I(+),SIGI(+),I(-),SIGI(-)" wavelength="${wavelength}" &>french_wilson.log
    ##the following is for data which do not have wavelength information
#    phenix.french_wilson "${targetPDBID}""_""${diffraction_ID}"".mtz" labels="I(+),SIGI(+),I(-),SIGI(-)" wavelength="${wavelength}" &>french_wilson.log

    ## the following line is for data from Melanie, which has wavelength information
    phenix.french_wilson "${targetPDBID}""_""${diffraction_ID}"".mtz" labels="I(+),SIGI(+),I(-),SIGI(-)" &>french_wilson.log
    mv french_wilson.mtz "${targetPDBID}""_""${diffraction_ID}""_french_wilson.mtz"
    echo "redirectedOutput ${targetPDBID}""_""${diffraction_ID}"
  else
    # It's a directory!
    # Directory command goes here.
    echo "not_present_dir_redirectedOutput ${targetPDBID} "
fi

