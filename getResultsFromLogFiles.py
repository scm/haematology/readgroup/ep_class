
from __future__ import division, print_function
import time, os, sys, shutil, glob, re, numpy as np
import subprocess as sub
import data_generation_class.class_definitions as d_gen_class

if (__name__ == "__main__"):
    try:
      placeHolder = sys.argv[1]
      # print ("the placeHolder is", placeHolder)
      targetPDBID = placeHolder.rsplit(",")[0].rsplit("_")[0]
      diffraction_ID = placeHolder.rsplit(",")[0].rsplit("_")[1]
      wavelength = float(placeHolder.rsplit(",")[1])
      substructure_atom_type = placeHolder.rsplit(",")[2].rsplit("/")[0]
      f_prime = placeHolder.rsplit(",")[3]
      f_double_prime = placeHolder.rsplit(",")[4]
    except Exception as e:
      sys.exit(-42)
    #print (targetPDBID, diffraction_ID, wavelength, substructure_atom_type, f_prime, f_double_prime)
    emptystr = ""


    sourcePath = os.path.join("/mnt/apollo/homes/ksh40/phassade/inputData/kau_dir", targetPDBID)

    if not os.path.exists(sourcePath):
        print("noPath_redirectedOutput", targetPDBID + "_" + diffraction_ID, round(wavelength, 4),
              substructure_atom_type)
    else:
        ksh_destinationPath = os.path.join("/mnt/apollo/homes/ksh40/phassade/work/workingDir")
        destinationPath = os.path.join("/mnt/apollo/homes/rdo20/MRSAD_Calc/phassade/workingDir")
        runDetails = sys.argv[2]

        if not os.path.exists(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID)):
            os.mkdir(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID))
        os.chdir(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID))

        if not os.path.exists(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID, "phasertng_run")):
            os.mkdir(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID, "phasertng_run"))
        os.chdir(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID, "phasertng_run"))

        runpath = os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID, "phasertng_run", runDetails)
        if not os.path.exists( runpath ):
          #print( targetPDBID + "_" + diffraction_ID, emptystr, emptystr,
          #      emptystr, emptystr, emptystr, emptystr, emptystr, emptystr, emptystr, emptystr, emptystr, emptystr, 
          #      emptystr, emptystr, emptystr, emptystr, emptystr, emptystr, emptystr, -43)
          sys.exit(-43)
        #    shutil.rmtree(runpath)
        #os.mkdir(runpath)
        os.chdir(runpath)

        #### setting variables to zero
        # Estimated_total_information_bits = Estimated_total_information_nats = 0.0
        # Estimated_total_information_in_reflections = average_bits_per_reflection = delta_bfactor = 0.0
        estimated_atom_number = strongest_ano_scatterer = 0.0
        f_prime = f_double_prime = anisotropy_bfactor = high_resolution = 0.0
        Estimated_total_information = SAD_information = emptystr

        ### to grep after running "further" logging mode ###
        # try:
        #print (os.getcwd())
        for file_name in glob.glob('phassade*[0-9]*.sca.dag'):
            #print("file_name is here: XXXXXXXXXXXXXXXXXXXX", file_name)
            with open(file_name, 'r') as output:
                fileContent=output.read().splitlines()
                # print ("this has been run")
                i=0
                for j in fileContent:
                    i = i+1
                    if "phasertng node molecular_weight" in j:
                        molecular_weight=float(j.rsplit()[-1])
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ))
        with open("phassade.log", 'r') as output:
            fileContent = output.read()
            # print ("this has been run")
            val = re.findall("phasertng [\ \. _]+ substructure [\ \. _]+ content [\ \. _]+ analysis [\ \. _]+ delta [\ \. _]+ bfactor [\ \. _]+ ( \d+ \. \d* ) ",
                                        fileContent, re.VERBOSE)
            delta_bfactor = val[0] if val != [] else emptystr

            val = re.findall("phasertng [\ \. _]+ anisotropy [\ \. _]+ delta [\ \. _]+ bfactor [\ \. _]+ ( \d+ \. \d* ) ",
                                        fileContent, re.VERBOSE)
            anisotropy_bfactor = val[0] if val != [] else emptystr
        
            val = re.findall("phasertng [\ \. _]+ anisotropy [\ \. _]+ outer [\ \. _]+ bin [\ \. _]+ contrast [\ \. _]+ ( \d+ \. \d* ) ",
                                        fileContent, re.VERBOSE)
            anisotropy_binContrast = val[0] if val != [] else emptystr
             
            tncs_present = True        
            val = re.findall("(No \s+ tNCS \s+ found \s+ in \s+ Patterson)", fileContent, re.VERBOSE)
            if val != []:
               tncs_present = False
        
            val = re.findall("phasertng [\ \. _]+ substructure [\ \. _]+ content [\ \. _]+ analysis [\ \. _]+ number [\ \. _]+ ( \d+ \. \d* ) ",
                                        fileContent, re.VERBOSE)
            estimated_atom_number = val[0] if val != [] else emptystr
        
            val = re.findall("phasertng [\ \. _]+ information [\ \. _]+ sad [\ \. _]+ expected [\ \. _]+ llg [\ \. _]+ ( \d+ \. \d* ) ",
                                        fileContent, re.VERBOSE)
            SAD_eLLG = val[0] if val != [] else emptystr
        
            val = re.findall("phasertng [\ \. _]+ information [\ \. _]+ sad [\ \. _]+ information [\ \. _]+ content [\ \. _]+ ( \d+ \. \d* ) ",
                                        fileContent, re.VERBOSE)
            SAD_information = val[0] if val != [] else emptystr
        
            val = re.findall("phasertng [\ \. _]+ information [\ \. _]+ information [\ \. _]+ content [\ \. _]+ ( \d+ \. \d* ) ",
                                        fileContent, re.VERBOSE)
            Estimated_total_information = val[0] if val != [] else emptystr

            val = re.findall("Maximum\ SAD\ eFOM .+ \n \s* average \s+ of \s+ (\d+ \. \d*) \s+ per \s+ reflection", fileContent, re.VERBOSE)
            Avg_SAD_eFOM_reflection = val[0] if val != [] else emptystr

            val = re.findall("\s+ Overall \s+ expected \s+ mapCC: \s+ (\d+ \. \d*)", fileContent, re.VERBOSE)
            Overall_eMapCC = val[0] if val != [] else emptystr

            val = re.findall("\s+ Fraction \s+ of \s+ weak \s+ anomalous \s+ data \s+ with \s+ Z\(power\) \s+ < \s+ 2\: \s+ ( \d+ \. \d* ) ",
                                        fileContent, re.VERBOSE)
            fracdatZ2 = val[0] if val != [] else emptystr

            val = re.findall("\s+ Highest \s+ resolution \s+ with \s+ Z\(power\) \s+ > \s+ 3\: \s+ ( \d+ \. \d* ) ",
                                        fileContent, re.VERBOSE)
            highresZ3 = val[0] if val != [] else emptystr

            val = re.findall("\s+ RMS \s+ value \s+ of \s+ Z\(power\)\: \s+ ( \d+ \. \d* ) ",
                                        fileContent, re.VERBOSE)
            rmsvalZ = val[0] if val != [] else emptystr

            val = re.findall("\s* Exit \s+ Code\: \s+ ( \d+ ) ",
                                        fileContent, re.VERBOSE)
            exitcode = val[0] if val != [] else emptystr



        file_names = glob.glob('phassade.*.sca.dag.phil')
        twinning = False
        if file_names:
            with open(file_names[0], 'r') as output:
                fileContent = output.read()
                val = re.findall("phasertng [\ \. _]+ node [\ \. _]+ twinned \s* = \s* ( \S* )", fileContent, re.VERBOSE)
                if val != []:
                   twinning = val[0]
        fileContent = ""
        PerfectHA_phaserlogfname = os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID, "positive_control",
                              "EP_SAD_withPerfectHa_selectedAtomType_fromRefinedModel_withLLGcompletion",
                              targetPDBID + "_" + diffraction_ID + ".log")
        if not os.path.exists(PerfectHA_phaserlogfname):
          PerfectHA_phaserlogfname = os.path.join(ksh_destinationPath, targetPDBID + "_" + diffraction_ID, "positive_control",
                                "EP_SAD_withPerfectHa_selectedAtomType_fromRefinedModel_withLLGcompletion",
                                targetPDBID + "_" + diffraction_ID + ".log")
        if os.path.exists(PerfectHA_phaserlogfname):
          #print(PerfectHA_phaserlogfname)
          with open(PerfectHA_phaserlogfname,"r") as output:
            fileContent = output.read()
        val = re.findall(r""" \s+ Figures \s+ of \s+ Merit \s* 
                    \s+ ---------------- \s* 
                    \s+  Bin \s+ Resolution \s+ Acentric \s+ Centric \s+ Single \s+ Total \s* 
                    \s+ Number \s+ FOM \s+ Number \s+ FOM \s+ Number \s+ FOM \s+ Number \s+ FOM \s* 
                    \s+ ALL \s+ \d+\.*\d*  \s* - \s* \d+\.\d* \s+ \d+ \s+ \d+\.*\d* \s+ \d+ \s+ \d+\.*\d* \s+ \d+ \s+ \d+\.*\d* \s+ \d+ \s+ (\d+\.*\d*) \s* 
                  """, fileContent,  re.VERBOSE)
        FOMvalue = val[0] if val != [] else emptystr

        PerfectHA_CClogfname = os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID, "positive_control",
                              "EP_SAD_withPerfectHa_selectedAtomType_fromRefinedModel_withLLGcompletion",
                              "cc_MTZ_PDB.log")
        if not os.path.exists(PerfectHA_CClogfname):
          PerfectHA_CClogfname = os.path.join(ksh_destinationPath, targetPDBID + "_" + diffraction_ID, "positive_control",
                                "EP_SAD_withPerfectHa_selectedAtomType_fromRefinedModel_withLLGcompletion",
                                "cc_MTZ_PDB.log")
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ))
        if os.path.exists(PerfectHA_CClogfname):
          #print(PerfectHA_CClogfname)
          with open(PerfectHA_CClogfname,"r") as output:
            fileContent = output.read()
        val = re.findall(r"\s*overall \s+ CC: \s+ (\d+\.*\d*) \s*", fileContent, re.VERBOSE)
        CCvalue = val[0] if val != [] else emptystr

        HA_noLLGcompletionfname = os.path.join(ksh_destinationPath, targetPDBID + "_" + diffraction_ID, "positive_control",
                              "EP_SAD_withPerfectHa_fromRefinedModel_withoutLLGcompletion_fullResolution",
                              targetPDBID + "_" + diffraction_ID + ".log")
        if os.path.exists(HA_noLLGcompletionfname):
          with open(HA_noLLGcompletionfname,"r") as output:
            fileContent = output.read()

        val = re.findall(r"\s*Final \s+ Log-Likelihood \s+ = \s+ (\d+\.*\d*) \s*", fileContent, re.VERBOSE)
        LLGval_nocompletion = val[0] if val != [] else emptystr

        CC_noLLGcompletionfname = os.path.join(ksh_destinationPath, targetPDBID + "_" + diffraction_ID, "positive_control",
                              "EP_SAD_withPerfectHa_fromRefinedModel_withoutLLGcompletion_fullResolution",
                              "cc_MTZ_PDB.log")
        if os.path.exists(CC_noLLGcompletionfname):
          with open(CC_noLLGcompletionfname,"r") as output:
            fileContent = output.read()

        val = re.findall(r"\s*overall \s+ CC: \s+ (\d+\.*\d*) \s*", fileContent, re.VERBOSE)
        CCval_nocompletion = val[0] if val != [] else emptystr
        lst = [targetPDBID + "_" + diffraction_ID, str(round(wavelength, 4)), substructure_atom_type,
            delta_bfactor, estimated_atom_number, SAD_eLLG, anisotropy_bfactor, anisotropy_binContrast,
            Estimated_total_information, SAD_information, str(tncs_present), str(twinning), Avg_SAD_eFOM_reflection, 
            Overall_eMapCC, FOMvalue, CCvalue, fracdatZ2, rmsvalZ, highresZ3, LLGval_nocompletion, 
            CCval_nocompletion, exitcode]
        print(",".join(lst))
