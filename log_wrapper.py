from __future__ import division, print_function
import time, os, sys, glob, numpy as np
import subprocess as sub

from EP_class import data_preparation_3Sep19 as data_prep


if (__name__ == "__main__"):

    placeHolder=sys.argv[1]
    # print ("the placeHolder is", placeHolder)
    targetPDBID=placeHolder.rsplit(",")[0].rsplit("_")[0]
    diffraction_ID=placeHolder.rsplit(",")[0].rsplit("_")[1]
    wavelength=float(placeHolder.rsplit(",")[1])
    substructure_atom_type=placeHolder.rsplit(",")[2].rsplit("/")[0]
    # high_resolution=float(placeHolder.rsplit(",")[2])
    # knownSites=int(placeHolder.rsplit(",")[3])
    summary=[]

    summary.append(targetPDBID+"_"+diffraction_ID)

    sourcePath=os.path.join("/home/ksh40/work/phassade/dataDump/kau_dir",targetPDBID)

    if not os.path.exists(sourcePath):
        print ("noPath_redirectedOutput",targetPDBID+"_"+diffraction_ID,round(wavelength,4),substructure_atom_type)
    else:
        # print ("pathExists_redirectedOutput",targetPDBID+"_"+diffraction_ID,round(wavelength,4),substructure_atom_type)


        # perfect_ha_substructure=os.path.join(sourcePath,"perfect_ha.pdb")

        # os.chdir(os.path.join(sourcePath,diffraction_ID))
        #
        # data_prep.Substructure.generate_structure_factors(perfect_ha_substructure, high_resolution,targetPDBID,diffraction_ID)
        # perfect_patterson_high_resolution_mtz="perfect_patterson_high_resolution.mtz"
        # data_prep.Substructure.generate_patterson_map(perfect_patterson_high_resolution_mtz)



        # perfect_patterson_high_resolution=os.path.join(sourcePath,diffraction_ID, "perfect_patterson_high_resolution.ccp4")


        destinationPath=os.path.join("/home/ksh40/work/phassade/workingDir")
        runDetails="11Nov2019_SCA"

        if not os.path.exists(os.path.join(destinationPath,targetPDBID+"_"+diffraction_ID)):
            os.mkdir(os.path.join(destinationPath,targetPDBID+"_"+diffraction_ID))
            os.chdir(os.path.join(destinationPath,targetPDBID+"_"+diffraction_ID))
        else:
            os.chdir(os.path.join(destinationPath,targetPDBID+"_"+diffraction_ID))


        if not os.path.exists(os.path.join(destinationPath,targetPDBID+"_"+diffraction_ID,runDetails)):
            os.mkdir(os.path.join(destinationPath,targetPDBID+"_"+diffraction_ID,runDetails))
            os.chdir(os.path.join(destinationPath,targetPDBID+"_"+diffraction_ID,runDetails))
        else:
            os.chdir(os.path.join(destinationPath,targetPDBID+"_"+diffraction_ID,runDetails))


        ###creating dir to run EP_SAD  ###
        if not os.path.exists(os.path.join(destinationPath,str(targetPDBID)+"_"+str(diffraction_ID),runDetails,"EP_SAD")):
            os.mkdir(os.path.join(destinationPath,str(targetPDBID)+"_"+str(diffraction_ID),runDetails,"EP_SAD"))
            os.chdir(os.path.join(destinationPath,str(targetPDBID)+"_"+str(diffraction_ID),runDetails,"EP_SAD"))
        else:
            os.chdir(os.path.join(destinationPath,str(targetPDBID)+"_"+str(diffraction_ID),runDetails,"EP_SAD"))


        # # ###creating dir to run MR_SAD ###
        # if not os.path.exists(os.path.join(destinationPath,str(targetPDBID)+"_"+str(diffraction_ID),runDetails,"MR_SAD")):
        #     os.mkdir(os.path.join(destinationPath,str(targetPDBID)+"_"+str(diffraction_ID),runDetails,"MR_SAD"))
        #     os.chdir(os.path.join(destinationPath,str(targetPDBID)+"_"+str(diffraction_ID),runDetails,"MR_SAD"))
        # else:
        #     os.chdir(os.path.join(destinationPath,str(targetPDBID)+"_"+str(diffraction_ID),runDetails,"MR_SAD"))

        # ###calling Phassade from phasertng to place single atom ###
        if not os.path.exists(os.path.join(destinationPath,str(targetPDBID)+"_"+str(diffraction_ID),runDetails,"phassade_tng")):
            os.mkdir(os.path.join(destinationPath,str(targetPDBID)+"_"+str(diffraction_ID),runDetails,"phassade_tng"))
            os.chdir(os.path.join(destinationPath,str(targetPDBID)+"_"+str(diffraction_ID),runDetails,"phassade_tng"))
        else:
            os.chdir(os.path.join(destinationPath,str(targetPDBID)+"_"+str(diffraction_ID),runDetails,"phassade_tng"))

        # print (targetPDBID+"_"+diffraction_ID,os.path.join(sourcePath,targetPDBID+".pdb"), os.path.join(sourcePath,"perfect_ha.pdb"),os.path.join(sourcePath,diffraction_ID,targetPDBID+"_"+diffraction_ID+".mtz"),os.path.join(sourcePath,targetPDBID+".fa"),substructure_atom_type,wavelength)
        #
        # with open("EP_SAD.log", 'w') as output:
        # 	runPhassade=sub.Popen(
        #             ['phenix.python','/home/ksh40/work/trial_runs/dataPreparation/EP_class/data_preparation_3Sep19.py', placeHolder],
        #         #shell=True,
        #            # stdin=echo.stdout,
        #             stdout=output,
        #         )
        # runPhassade.communicate()



        # spa_mode_patterson=glob.glob("*spa.map")[0]
        #
        # mapCC=data_prep.Substructure.runCC_MTZ_MTZ(perfect_patterson_high_resolution, spa_mode_patterson)
        #
        # # print ("redirectedOutput", targetPDBID+"_"+diffraction_ID, mapCC)
        negativeFound=False
        error_type=False

        #
        try:
            with open("phassade.log",'r') as output:
                fileContent=output.read().splitlines()
                i=0
                for j in fileContent:
                    i=i+1
                    if "Information Content: SAD Extra Data" in j:
                        # print ("XXXXXXXXXXXXXX entering the trial XXXXXXXXXXXXXX")
                        # print (fileContent[i+7])
                        # print ("XXXXXXXXXXXXXX entering the trial XXXXXXXXXXXXXX")
                        # substructure_content_analysis_number=float(fileContent[i-4].rsplit()[6])
                        Estimated_total_information_bits=float(fileContent[i+7].rsplit()[4])
                        Estimated_total_information_nats=float(fileContent[i+7].rsplit()[7])
                        Estimated_total_information_in_reflections=float(fileContent[i+7].rsplit()[10])
                        average_bits_per_reflection=float(fileContent[i+10].rsplit()[2])
                    elif "phasertng tncs_analysis indicated" in j:
                        tNCS_type=str(j.rsplit()[-1])
                    elif "substructure_content_analysis delta_bfactor" in j:
                        delta_bfactor=float(j.rsplit()[-1])
                    elif "substructure_content_analysis number" in j:
                        estimated_atom_number=float(j.rsplit()[-1])
                    elif "phasertng anisotropy delta_bfactor" in j:
                        anisotropy_bfactor=float(j.rsplit()[-1])
                    elif "phasertng data resolution_available" in j:
                        high_resolution=float(j.rsplit()[-2])
                        low_resolution=float(j.rsplit()[-1])
                    elif "SIGI(+) is negative" in j:
                        negativeFound=True
                    elif "SIGI(-) is negative" in j:
                        negativeFound=True
                    elif "ERROR" in j:
                        error_type=True
                        error_type_txt=j



            if negativeFound:
                print ("negativeFound_redirectedOutput",targetPDBID+"_"+diffraction_ID,round(wavelength,4),substructure_atom_type)
            elif error_type:
                print ("errorType_redirectedOutput",targetPDBID+"_"+diffraction_ID,round(wavelength,4),substructure_atom_type,error_type_txt)
            else :
                print ("redirectedOutput", targetPDBID+"_"+diffraction_ID,round(wavelength,4),substructure_atom_type,Estimated_total_information_bits,Estimated_total_information_nats,Estimated_total_information_in_reflections,average_bits_per_reflection,delta_bfactor,estimated_atom_number,anisotropy_bfactor,high_resolution,low_resolution,tNCS_type)
        except :
            print ("notRUN_redirectedOutput",targetPDBID+"_"+diffraction_ID,round(wavelength,4),substructure_atom_type)
