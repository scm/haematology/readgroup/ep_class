from __future__ import division, print_function
import time, os, sys, glob, numpy as np
from itertools import count
import subprocess as sub
import shutil
# from iotbx import crystal_symmetry_from_any
from iotbx.pdb import hierarchy
import functions.functions_library as func_lib
import data_generation_class.class_definitions as d_gen_class
import reflection_data_manipulation.reflection_data_manipulation_functions as data_manipulation

if (__name__ == "__main__"):
    # func_lib.replace_atomType("/home/ksh40/temp/data/substructure.pdb", "I")
    # object_holder = d_gen_class.Phasing_methods("6JR4" + "_" + "w1",
    #                                             "/home/ksh40/temp/data/6JR4.pdb",
    #                                             "/home/ksh40/temp/data/substructure.pdb",
    #                                             "/home/ksh40/temp/data/6JR4.mtz",
    #                                             "/home/ksh40/temp/data/6JR4.fa",
    #                                             "AG", 2.0)
    #
    # object_holder.runEP_Auto()

    placeHolder = sys.argv[1]
    print(placeHolder)
    phasingModelPDBID = str(placeHolder.rsplit(',')[0])
    targetPDBID = str(placeHolder.rsplit(',')[1])
    diffraction_ID = "w1"  # temp variable holder. Never really used.
    substructure_atom_type = "SE"  # temp variable holder. Never really used.
    gonnetScore = float(placeHolder.rsplit(',')[2])
    TargetResolution = float(placeHolder.rsplit(',')[3])
    phasingModelPDBID_chainLength = float(placeHolder.rsplit(',')[4])
    Molprobity = float(placeHolder.rsplit(',')[5])
    MRsummaryResult = []

    eVRMS = func_lib.eVRMS_Xray(phasingModelPDBID_chainLength, gonnetScore, Molprobity, TargetResolution)
    print("the eVRMS of " + targetPDBID + "_" + phasingModelPDBID + " is " + str(eVRMS))

    sourcePath = os.path.join("/mnt/apollo/homes/ksh40/dataDump2/1component", targetPDBID)  # run from techne
    destinationPath = os.path.join("/mnt/apollo/homes/ksh40/MR_project/bulk_solvent/trials")  # run from techne

    if not os.path.exists(os.path.join(sourcePath, targetPDBID, targetPDBID.lower() + "_map.1.mtz")):
        targetMapMtz = os.path.join(sourcePath, targetPDBID, targetPDBID.upper() + "_map.1.mtz")
    else:
        targetMapMtz = os.path.join(sourcePath, targetPDBID, targetPDBID.lower() + "_map.1.mtz")

    os.chdir(os.path.join(sourcePath))
    coordName = glob.glob("*.pdb")[0]

    os.chdir(os.path.join(sourcePath, "Phaserinput"))
    seqName = glob.glob("*.seq")[0]
    mtzName = glob.glob("*.mtz")[0]
    targetPDBID_seqFile = os.path.join(sourcePath, "Phaserinput", seqName)
    targetPDBID_mtzFile = os.path.join(sourcePath, "Phaserinput", mtzName)
    targetPDBID_coordFile = os.path.join(sourcePath, coordName)

    phasingModelPDBID_filePath = os.path.join(sourcePath, "MR", phasingModelPDBID, "RNP_VRMS.1.pdb")

    # running sculpted and merged model
    if not os.path.exists(os.path.join(destinationPath, targetPDBID)):
        os.mkdir(os.path.join(destinationPath, targetPDBID))
    os.chdir(os.path.join(destinationPath, targetPDBID))

    if not os.path.exists(os.path.join(destinationPath, targetPDBID, phasingModelPDBID)):
        os.mkdir(os.path.join(destinationPath, targetPDBID, phasingModelPDBID))
    os.chdir(os.path.join(destinationPath, targetPDBID, phasingModelPDBID))

    object_holder = d_gen_class.Phasing_methods(targetPDBID + "_" + diffraction_ID,
                                                targetPDBID_coordFile,  # fully refined full model
                                                targetPDBID_coordFile,  # substructure model
                                                targetPDBID_mtzFile,
                                                targetPDBID_seqFile,
                                                substructure_atom_type, wavelength=None)

    print("XXXXXXXXXXXXXXXX")
    print("Running phaser RNP with grid search")
    print("XXXXXXXXXXXXXXXX")
    ###starting loop over FSOL
    tag = "SigmaA_FSOL"
    for SigmaA_FSOL_value in np.arange(0.1, 0.5, 0.1):
        tag = tag + str(np.round(SigmaA_FSOL_value, 2))
        tagInner = tag + "SigmaA_BSOL"

        ###starting loop over BSOL
        for SigmaA_BSOL_value in np.arange(0.1, 20, 20):
            tagInner = tagInner + str(np.round(SigmaA_BSOL_value, 2))

            if not os.path.exists(os.path.join(destinationPath, targetPDBID, phasingModelPDBID, "phaser" + tagInner)):
                os.mkdir(os.path.join(destinationPath, targetPDBID, phasingModelPDBID, "phaser" + tagInner))
            os.chdir(os.path.join(destinationPath, targetPDBID, phasingModelPDBID, "phaser" + tagInner))

            SigmaA_FSOL_runInfo, SigmaA_BSOL_runInfo, BulkSolvent_FSOL_runInfo, BulkSolvent_BSOL_runInfo, rVRMS, rLLGI, rTFZ = object_holder.runRNP(phasingModelPDBID_filePath,
                                                                                  phasingModelPDBID, eVRMS,
                                                                                  SigA_FSOL=SigmaA_FSOL_value,
                                                                                  SigA_BSOL=SigmaA_BSOL_value,
                                                                                  BulkSolvent_FSOL=0.4,
                                                                                  BulkSolvent_BSOL=80,
                                                                                  six_dimensions = None)

            print('current working dir is ', os.getcwd())

            solutionMTZ = glob.glob("*.mtz")[0]
            globalCC_value = d_gen_class.Substructure.runCC_MTZ_PDB(solutionMTZ, targetPDBID_coordFile, globalCC=True)

            print ("redirectedOutput", targetPDBID, phasingModelPDBID, eVRMS, SigmaA_FSOL_runInfo, SigmaA_BSOL_runInfo, BulkSolvent_FSOL_runInfo, BulkSolvent_BSOL_runInfo,
                   rVRMS, rLLGI,
                   rTFZ,
                   globalCC_value,
                   tagInner)

            try:
                shutil.rmtree(os.path.join(destinationPath, targetPDBID, phasingModelPDBID, "phaser" + tagInner,
                                           "temp_dir"))
                shutil.rmtree(os.path.join(destinationPath, targetPDBID, phasingModelPDBID, "phaser" + tagInner,
                                           "offset.pdb"))
                
                shutil.rmtree(solutionMTZ)

            except:
                pass
            os.chdir(os.path.join(destinationPath, targetPDBID, phasingModelPDBID))
            tagInner = tag + "SigmaA_BSOL"
        tag = "SigmaA_FSOL"
