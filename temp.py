import numpy as np
import os

print("XXXXXXXXXXXXXXXX")
print("Running phaser RNP with grid search")
print("XXXXXXXXXXXXXXXX")
tag = "_FSOL"
for FSOL_value in np.arange(0.1, 0.4, 0.1):
    tag = tag + str(np.round(FSOL_value, 2))
    tagInner = tag + "_BSOL"

    for BSOL_value in np.arange(50, 150, 50):
        tagInner = tagInner + str(np.round(BSOL_value, 2))
        print ("This is tagInner", tagInner)
        tagInner = tag + "_BSOL"
    tag = "_FSOL"
