from __future__ import division, print_function

from time import sleep

from phaser import SimpleFileProperties
import os, sys
from iotbx.file_reader import any_file
from iotbx.reflection_file_reader import any_reflection_file
from cctbx import miller
from cctbx import crystal
import glob
import subprocess as sub


def splitMTZ(mtz_file, targetPDBID, crystalID):
    """
    This program will split the MTZ file into several MTZ files depending on the number
    of diffraction datasets present in the single MTZ file
    """
    crystalID = int (crystalID)
    # diffractionID = []
    # diffractionID.append("redirectedOutput")
    hkl_in = any_reflection_file(file_name=mtz_file)
    miller_arrays = hkl_in.as_miller_arrays()

    for f in miller_arrays:
        # print (f.show_summary())
        templabels = str(f.info())
        # print(templabels)
        I = templabels.rsplit(':')[1].rsplit(',')[0]
        # print ("I is as follows", I)
        if I == "I(+)" or I == "I(+)_WAVE"+str(crystalID):
            intensity_array = f
            mtz_dataset = intensity_array.as_mtz_dataset("I")
            if not os.path.exists(os.path.join("w" + str(crystalID))):
                os.mkdir("w" + str(crystalID))
            os.chdir("w" + str(crystalID))
            mtz_dataset.mtz_object().write(targetPDBID + "_w" + str(crystalID) + ".mtz")

            #     os.mkdir("w" + str(crystalID))
            #     os.chdir("w" + str(crystalID))
            #     print("XXXXXXXX", I)
            #     print(I[0][:-3])
            #     mtz_dataset = f.as_mtz_dataset(column_root_label=I[0][:-3])
            #     # print ("XXXXXXXXprinting mtz_dataset")
            #     # print (dir(mtz_dataset))
            #     # print(mtz_dataset.column_types())
            #     wavelength = mtz_dataset.wavelength()
            #     # print ("XXXXXXXX")
            #     f_obs = f.french_wilson()
            #     # print ("XXXXXXXXXXXX printing f_obs XXXXXXXXX")
            #     # # print (dir(f))
            #     # print (f_obs.observation_type())
            #     # print ("XXXXXXXXXXXXXXXXXXXXX")
            #     mtz_dataset.add_miller_array(f_obs, column_root_label="F")
            #     f_merged = f.average_bijvoet_mates()
            #     mtz_dataset_merged = f_merged.as_mtz_dataset(column_root_label=I[0][:-3])
            #     print("XXXXXXXXprinting f")
            #     print(f_merged.info())
            #     print("XXXXXXXX")
            #
            #     mtz_dataset.mtz_object().write(targetPDBID + "_w" + str(crystalID) + ".mtz")
            #     mtz_dataset_merged.mtz_object().write(targetPDBID + "_w" + str(crystalID) + "_merged.mtz")
            #
            #     diffractionID.append(targetPDBID + "_w" + str(crystalID))
            #     diffractionID.append(wavelength)
            #     crystalID = crystalID + 1
            #     os.chdir('..')
            #     print(diffractionID)
            #     diffractionID = []
            #     diffractionID.append("redirectedOutput")


def check_column_type(MTZ_file):
    """
    returns column names in the MTZ file
    :param MTZ_file: MTZ file
    :return: list of column names in the MTZ file
    """
    MTZobj = SimpleFileProperties.GetMtzColumnsTuple(MTZ_file)
    labels = MTZobj[1]
    # print(MTZ_file)
    return labels


def read_structure_factor(targetPDBID_structure_factor):
    """

    :param targetPDBID_structure_factor:targetPDBID-sf.cif file as input
    :return:
    """
    structure_factor_file = targetPDBID_structure_factor
    with open(structure_factor_file, 'r') as output:
        fileContent = output.read().splitlines()
        i = 0
        for j in fileContent:
            if "_diffrn_radiation_wavelength.wavelength" in j:
                diffraction_wavelength = float(j.rsplit()[1])
            elif "_cell.length_a" in j:
                length_a = float(j.rsplit()[1])
            elif "_cell.length_b" in j:
                length_b = float(j.rsplit()[1])
            elif "_cell.length_c" in j:
                length_c = float(j.rsplit()[1])
            elif "_cell.angle_alpha" in j:
                angle_alpha = float(j.rsplit()[1])
            elif "_cell.angle_beta" in j:
                angle_beta = float(j.rsplit()[1])
            elif "_cell.angle_gamma" in j:
                angle_gamma = float(j.rsplit()[1])
            elif "_symmetry.space_group_name_H-M" in j:
                spacegroup_name = j.rsplit("\"")[1].replace(" ", "")
            elif "_symmetry.Int_Tables_number" in j:
                spacegroup_number = int(j.rsplit()[1])

    return diffraction_wavelength, length_a, length_b, length_c, angle_alpha, angle_beta, angle_gamma, spacegroup_name, spacegroup_number


def read_data_from_MTZ(targetPDBID_MTZ_format):
    """
    returns column names in the MTZ file
    :param targetPDBID_MTZ_format:
    :param MTZ_file: MTZ file
    :return: list of column names in the MTZ file
    """
    # MTZ_file = targetPDBID_MTZ_format
    # with open(targetPDBID+"_mtzdump.log", 'w') as output:
    # 	runMTZdump=sub.Popen(
    #             ['phenix.mtz.dump', MTZ_file],
    #         #shell=True,
    #            # stdin=echo.stdout,
    #             stdout=output,
    #         )
    # runMTZdump.communicate()
    #
    af = any_file(targetPDBID_MTZ_format)
    data_content = af.file_content.file_content()

    # return data_content.max_min_resolution(), data_content.column_labels()
    # min_max, column_label = read_data_from_MTZ(targetPDBID)
    if 'I(+)' in data_content.column_labels():
        print("I_plus_minus", data_content.max_min_resolution()[0], data_content.max_min_resolution()[1],
              data_content.space_group_number(), data_content.space_group_name())
        return ("I_plus_minus", data_content.max_min_resolution()[0], data_content.max_min_resolution()[1],
                data_content.space_group_number(), data_content.space_group_name())
    elif 'F(+)' in data_content.column_labels():
        print("F_plus_minus", data_content.max_min_resolution()[0], data_content.max_min_resolution()[1],
              data_content.space_group_number(), data_content.space_group_name())
        return ("F_plus_minus", data_content.max_min_resolution()[0], data_content.max_min_resolution()[1],
                data_content.space_group_number(), data_content.space_group_name())
    else:
        print("no_Ano_signal")
        return ("no_Ano_signal")

    #
    # MTZobj = SimpleFileProperties.GetMtzColumnsTuple(MTZ_file)
    # labels=MTZobj[1]
    # # print(MTZ_file)
    # return labels

def read_french_wilson_log_file(french_wilsion_log_file):
    """
    The function reads french wilson file to grep wavelength information.
    :param french_wilsion_log_file: input french wilson log file
    :return: wavelength
    """
    wavelength = 0.00
    with open(french_wilsion_log_file) as output:
        fileContent = output.read().splitlines()
        for j in fileContent:
            if "Using wavelength" in j:
                # print(j)
                wavelength = float(j.rsplit()[1].rsplit("=")[-1])
                # print (wavelength)
    return wavelength
# def merge_anomalous_data(MTZ_file):

#
if (__name__ == "__main__"):
    read_data_from_MTZ("/home/ksh40/Documents/lysozyme-MRSAD_0/5OQD/5XFN.mtz")

    # read_french_wilson_log_file("/home/ksh40/work/phassade/dataDump/more_data/melanie_synchrotron/melanie_workingDir/5FII/w1/french_wilson.log")

#     placeHolder = sys.argv[1]
#     # targetPDBID = placeHolder
#     targetPDBID = placeHolder.rsplit(",")[0]
#     # ano_scatterer = placeHolder.rsplit(",")[1]
#     os.chdir("/home/ksh40/work/phassade/dataDump/more_data/temp_processing")
#     diffraction_wavelength = read_structure_factor(targetPDBID)
#
#     sleep(1)
#     # MTZ_file=targetPDBID+".mtz"
#
#     min_max, column_label = read_data_from_MTZ(targetPDBID)
#     if 'I(+)' in column_label:
#         print("IncludedData", placeHolder, diffraction_wavelength, "I_plus_minus")
#     elif 'F(+)' in column_label:
#         print("IncludedData", placeHolder, diffraction_wavelength, "F_plus_minus")
#     else:
#         print("DroppedData", placeHolder, diffraction_wavelength, "no_Ano_signal")
#
#     #
#     # if not labels:
#     #     print("XXXXXX not found XXXXX ", targetPDBID)
#     #     # os.remove(glob.glob[targetPDBID + "*"])
#     # elif 'I(+)' in labels:
#     #     print ("anomalous signal found for ", targetPDBID)
