from __future__ import division, print_function
import time, os, sys, glob, numpy as np
import subprocess as sub
from shutil import copyfile
import reflection_data_manipulation.reflection_data_manipulation_functions as data_manipulation
import functions.functions_library as functions_library

if __name__ == "__main__":
    placeHolder = sys.argv[1]
    # print (placeHolder)
    targetPDBID = placeHolder.rsplit(",")[0].rsplit("_")[0]
    diffraction_ID = placeHolder.rsplit(",")[0].rsplit("_")[1]
    # print(targetPDBID, diffraction_ID, substructure_atom_type)
    sourcePath = os.path.join("/home/ksh40/work/phassade/dataDump/kau_dir", targetPDBID, diffraction_ID)

    if os.path.exists(sourcePath):
        os.chdir(os.path.join(sourcePath))
        targetPDBID_MTZ_format = os.path.join(sourcePath, targetPDBID+"_" +diffraction_ID+".mtz")
        # targetPDBID_PDB_format = os.path.join(sourcePath, targetPDBID + ".pdb")
        # os.remove(targetPDBID + "_" + "perfect_ha" + "_" + substructure_atom_type + ".pdb")
        # os.remove(targetPDBID + "_" + "perfect_ha" + "_" + substructure_atom_type + ".cif")
        # num_of_disulfide = functions_library.check_for_disulfide(targetPDBID_PDB_format)
        # print ("redirectedOutput", targetPDBID, num_of_disulfide)
        functions_library.mtz2sca_conversion(targetPDBID_MTZ_format)
        print ("redirectedOutput_convertedToSca_fileFormat", targetPDBID)
    else:
        print("target_notPresent_redirectedOutput", targetPDBID)
