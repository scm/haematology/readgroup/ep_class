from __future__ import division, print_function
import time, os, sys, glob, numpy as np
import subprocess as sub
from shutil import copyfile
import reflection_data_manipulation.reflection_data_manipulation_functions as data_manipulation
import functions.functions_library as functions_library

if __name__ == "__main__":
    placeHolder = sys.argv[1]
    # print (placeHolder)
    targetPDBID = placeHolder.rsplit(",")[0].rsplit("_")[0]
    diffraction_ID = placeHolder.rsplit(",")[0].rsplit("_")[1]
    # print(targetPDBID, diffraction_ID, substructure_atom_type)
    sourcePath = os.path.join("/home/ksh40/work/phassade/dataDump/kau_dir/", targetPDBID)
    destination = os.path.join("/home/ksh40/Downloads/test_cases/")

    if os.path.exists(sourcePath):
        os.chdir(os.path.join(sourcePath))
        targetPDBID_MTZ_format = os.path.join(sourcePath, diffraction_ID, targetPDBID+"_" +diffraction_ID+".mtz")
        targetPDBID_seq_file = os.path.join(sourcePath, targetPDBID+".fa")
        targetPDBID_model_file = os.path.join(sourcePath, targetPDBID + ".pdb")

        os.mkdir(os.path.join(destination, targetPDBID+"_" +diffraction_ID))
        os.chdir(os.path.join(destination, targetPDBID+"_" +diffraction_ID))
        copyfile(targetPDBID_MTZ_format, os.path.join(destination, targetPDBID+"_" +diffraction_ID, targetPDBID+"_" +diffraction_ID+".mtz"))
        copyfile(targetPDBID_seq_file, os.path.join(destination, targetPDBID+"_" +diffraction_ID, targetPDBID+"_" +diffraction_ID+".fa"))
        copyfile(targetPDBID_model_file, os.path.join(destination, targetPDBID+"_" +diffraction_ID, targetPDBID+"_" +diffraction_ID+".pdb"))

        print ("redirectedOutput_convertedToSca_fileFormat", targetPDBID)
    else:
        print("target_notPresent_redirectedOutput", targetPDBID)
