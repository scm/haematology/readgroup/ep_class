install.packages("git2r")
install.packages("devtools")

devtools::install_github("nicolewhite/RNeo4j")

install.packages("ROCR")
install.packages("pROC")
install.packages("PerformanceAnalytics")
install.packages("reshape2")
install.packages("ggplot2")
install.packages("randomForest")
install.packages("wesanderson")#colour pallate 


library("PerformanceAnalytics")
library("reshape2")
library("ggplot2")
library("ROCR")
library("pROC")
library("randomForest")
library("devtools")
library("RNeo4j")
library("httr")
library("stringi")
library("wesanderson")#colour pallate 

graph = startGraph("http://localhost:7474/db/data/",
                   username="neo4j",
                   password="cimr6.49")

graph$version


"and not a.ano_atom_scatterer IN [\"BA\",\"CU\",\"TA\",\"NI\",\"TB\",\"V\",\"YB\",\"AS\",\"GD\",\"RB\",\"S/MG\",\"TA/BR\",\"ZN/AS\",\"HG/SR\",\"HG/I\"]"

#to generate supplement graphs
trial="MATCH 
(a)-[b:sca_withRefinedFDP_values_svn635]->(a),
(a)-[r:refined_withFW_svn635]->(a)
where not a.nativeSAD_verified IN ['UNSURE']
and r.localCC>0.2
and not a.targetPDBID_diffractionID starts with \"RADTHAUMATIN\"

return a.targetPDBID_diffractionID, a.ano_atom_scatterer as ano_atom_type,    
a.resolution as Resolution, a.lambda as Wavelength, 
r.selected_atoms_occupancy_squared_sum as Total_ano_scattering,
r.f_double_prime as f_double_prime,
r.localCC as map_to_model_correlation,
b.estimated_atom_number
"

table=cypher(graph, trial)


#density plot
t<-ggplot(data=melt(table[c(3:7)]), aes(x=value, fill=variable)) + 
    geom_density(alpha=.3) 
t+facet_wrap(.~variable, scales="free")+theme_minimal()+
    theme(legend.title=element_blank(),legend.position="none")+
    xlab("") + ylab("density") 
    
last_plot()+ggsave("/home/ksh40/work/phassade/SAD_info_content_paper/figures/Database_overview_properties.tiff", 
                   device="tiff", dpi=600,
                   width = 16, height = 12, units = c("cm"))


#scatter plot as function of ano_scatterer

# jinga<-melt(table[c(5,8)])

###scatter plot as function of scatterer type ###
t<-ggplot(data=table[c(2,6)], aes(x=table$Total_ano_scattering,
                                    y=table$b.estimated_atom_number)) +
    geom_point() +    # Use hollow circles
    geom_smooth(method=lm,   # Add linear regression line
                se=FALSE)    # Don't add shaded confidence region
t+facet_wrap(table$ano_atom_type~., scales="free")+
    xlab("Sum of squared refined occupancies") +
    ylab("Estimated number")
last_plot()+ggsave("/home/ksh40/work/phassade/SAD_info_content_paper/figures/EstimatedNumber_scatterplot_functionOfScatterer.tiff", 
                   device="tiff", dpi=600,
                   width = 16, height = 12, units = c("cm"))



chart.Correlation(table[c(4,5)], pch=19, method=c("spearman") )

##Scatter plot in log scale
plot1<-ggplot(data=table[c(5:7)], aes(x=table$Total_ano_scattering, 
                                y=table$b.estimated_atom_number))+
    geom_point(size=0.5)+
    geom_smooth(method=lm,   # Add linear regression line
                se=FALSE)+    # Don't add shaded confidence region
    scale_x_continuous(trans='log10',limits = c(0.1, 300)) +
    scale_y_continuous(trans='log10',limits = c(0.1, 300))+
    theme_minimal(base_size = 8)+ 
    ggtitle("A") 


## scatter plot as function of mapCC (which is measure of ano strength) ##
##plotting boxplot using automatic binning for Helix
# binSize=10
# plot1<-ggplot(table_Xray[c(5:7)], aes(table_Xray$Helix, table_Xray$Resolution))+
#     geom_boxplot(aes(group = cut_width(table_Xray$Helix, binSize)))+
#     scale_x_continuous(limits = c(0, 100),minor_breaks = seq(0 , 100, 5), 
#                        breaks = seq(0, 100, 10))+
#     theme_minimal()+
#     xlab("Percentage of Helix") + 
#     ylab("Resolution")
#write.csv(table,"/home/ksh40/work/phassade/SAD_info_content_paper/figures/SAD_data.csv")

table_binned<-read.csv("/home/ksh40/work/phassade/SAD_info_content_paper/figures/SAD_data.csv", header=TRUE)
t<-ggplot(data=table_binned, aes(x=table_binned$Total_ano_scattering, 
                               y=table_binned$b.estimated_atom_number))+
    geom_point(size=0.5)+
    geom_smooth(method=lm,   # Add linear regression line
                se=FALSE)+    # Don't add shaded confidence region
    scale_x_continuous(trans='log10',limits = c(0.1, 300)) +
    scale_y_continuous(trans='log10',limits = c(0.1, 300))
    
plot2<-t+facet_grid(.~table_binned$map_coarseBin, scales="free")+
    theme_minimal(base_size = 8)+
    theme(legend.title=element_blank(),legend.position="none")+
    xlab("Total anomalous scattering") + ylab("Estimated anomalous scattering") 

grid.arrange(plot1, plot2)
#save
g <- arrangeGrob(plot1, plot2) #generates g
ggsave("/home/ksh40/work/phassade/SAD_info_content_paper/figures/EstimatedNumber_functionOfMapCC.tiff", 
                   g,
                   device="tiff", dpi=600,
                   width = 8.85, height = 6, units = c("cm"))


### periodic table ###
#dd <- read.csv("/home/ksh40/work/phassade/SAD_info_content_paper/figures/elements.txt")
dd <- read.csv("/home/ksh40/work/phassade/SAD_info_content_paper/figures/elements.csv", header=TRUE)
# wrapper <- function(x, ...) paste(stri_wrap(x, ...), collapse = "\n")

ggplot(dd, aes(Column, -Row)) + 
    geom_tile(data=dd, aes(fill=GroupName), color="black") + 
    #scale_fill_manual(values=wes_palette(n=10, name="Moonrise1"))+
    scale_fill_brewer(palette="Set3")+
    # scale_fill_manual(values=c("#DCDCDC", "#DCDCDC", "#DCDCDC","#DCDCDC", "#DCDCDC", "#DCDCDC",
    #                            "#DCDCDC", "#DCDCDC", "#DCDCDC","#DCDCDC"))+
    geom_text(aes(label=stringr::str_wrap(DatabaseEntries, 3)),size=2.5, fontface = "bold")+
    theme_void()+
    theme(legend.position="bottom", 
          #legend.position="none", 
          legend.text = element_text(size=8))

last_plot()+ggsave("/home/ksh40/work/phassade/SAD_info_content_paper/figures/periodic_table_colour.tiff", 
                   device="tiff", dpi=600,
                   width = 18, height = 10, units = c("cm"))
