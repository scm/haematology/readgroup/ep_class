from __future__ import division, print_function
import time, os, sys, glob, numpy as np
import subprocess as sub
from shutil import copyfile
import reflection_data_manipulation.reflection_data_manipulation_functions as data_manipulation
import functions.functions_library as func_lib

if __name__ == "__main__":
    f_double_prime = anisotropy_bfactor = 0.0
    placeHolder = sys.argv[1]
    # print (placeHolder)
    targetPDBID = placeHolder.rsplit(",")[0].rsplit("_")[0]
    diffraction_ID = placeHolder.rsplit(",")[0].rsplit("_")[1]
    # wavelength = float(placeHolder.rsplit(",")[1])
    substructure_atom_type = placeHolder.rsplit(",")[2].rsplit("/")[0]

    print(targetPDBID, diffraction_ID, substructure_atom_type)
    # ### following is to get infomation from KAUSHIK data
    SAD_log = os.path.join("/home/ksh40/work/phassade/workingDir", targetPDBID+"_" +diffraction_ID, "20Nov2019_svn616", "EP_SAD", targetPDBID+"_" +diffraction_ID+".log")
    SAD_PDBID = os.path.join("/home/ksh40/work/phassade/workingDir", targetPDBID+"_" +diffraction_ID, "20Nov2019_svn616", "EP_SAD", targetPDBID+"_" +diffraction_ID+".1.pdb")

    # ### following is to get infomation from TOM data
    # SAD_log = os.path.join("/home/ksh40/work/phassade/workingDir", targetPDBID+"_" +diffraction_ID, "19Aug2019", "EP_SAD_v2", targetPDBID+"_" +diffraction_ID+".log")

    if os.path.exists(SAD_log):
        f_prime, f_double_prime = func_lib.read_EP_SAD_log(SAD_log)
        selected_atoms_occupancy, selected_atoms_occupancy_squared_sum, number_of_atoms = func_lib.calculate_total_occupancy(SAD_PDBID, substructure_atom_type)
        print("redirectedOutput", targetPDBID + "_" + diffraction_ID, selected_atoms_occupancy, selected_atoms_occupancy_squared_sum, number_of_atoms, f_prime, f_double_prime)



    # phassade_log = os.path.join("/home/ksh40/work/phassade/workingDir", targetPDBID+"_" +diffraction_ID, "24Oct2019_anodiff", "phassade_tng", "phassade.log")
    #
    # if os.path.exists(phassade_log):
    #     anisotropy_bfactor, f_double_prime = func_lib.read_phassade_log(phassade_log)
    #
    # if f_double_prime != 0:
    #     print ("redirectedOutput", targetPDBID+"_" +diffraction_ID, anisotropy_bfactor, f_double_prime)