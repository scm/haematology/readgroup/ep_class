from __future__ import division, print_function
import time, os, sys, glob, numpy as np
import subprocess as sub
from shutil import copyfile
import reflection_data_manipulation.reflection_data_manipulation_functions as data_manipulation

if __name__ == "__main__":
    placeHolder = sys.argv[1]
    # print (placeHolder)
    targetPDBID = placeHolder.rsplit(",")[0].rsplit("_")[0]
    diffraction_ID = placeHolder.rsplit(",")[0].rsplit("_")[1]
    crystalID = diffraction_ID[-1]
    # wavelength = float(placeHolder.rsplit(",")[1])
    # substructure_atom_type = placeHolder.rsplit(",")[2].rsplit("/")[0]

    # sourcePath = os.path.join("/home/ksh40/work/phassade/dataDump/kau_dir", targetPDBID)
    sourcePath = os.path.join("/home/ksh40/work/phassade/dataDump/more_data/melanie_synchrotron/data_files", targetPDBID)
    mtz_file = os.path.join("/home/ksh40/work/phassade/dataDump/more_data/melanie_synchrotron/data_files",
                            targetPDBID, "AUTOMATIC_DEFAULT_free.mtz")

    os.chdir(sourcePath)

    data_manipulation.splitMTZ(mtz_file, targetPDBID, crystalID)

    # if not os.path.exists(sourcePath):
    #     os.mkdir(sourcePath)
    # os.chdir(sourcePath)


    # os.chdir(os.path.join(sourcePath, diffraction_ID))
    # print(os.getcwd())
    # targetPDBID_MTZ_format = os.path.join(sourcePath, diffraction_ID, targetPDBID+"_" +diffraction_ID+".mtz")
    # # wavelength_info = " wavelength=" + str(wavelength)
    # # command_line_arguments = targetPDBID + "_" + diffraction_ID + ".mtz" + wavelength_info
    # # print("the command line argument is ", command_line_arguments)
    # # with open("french_wilson.log", 'w') as output:
    # #     french_wilson = sub.Popen(
    # #         ['phenix.french_wilson', targetPDBID_MTZ_format, wavelength_info],
    # #         stdout=output,
    # #     )
    # #     french_wilson.communicate()
    # # #
    # # # with open("french_wilson.log", 'w') as output:
    # # #     runFrenchWilson = sub.Popen(
    # # #         ['phenix.french_wilson', command_line_arguments],
    # # #         shell=True,
    # # #         # stdin=echo.stdout,
    # # #         stdout=output,
    # # #     )
    # # # runFrenchWilson.communicate()
    # # #
    # # copyfile("french_wilson.mtz", targetPDBID+"_" +diffraction_ID+"_french_wilson.mtz")
    # # os.remove("french_wilson.mtz")
    # #
    # # print("redirectedOutput_allDone", targetPDBID)
    #
    # # diffraction_wavelength, length_a, length_b, length_c, angle_alpha, angle_beta, angle_gamma, spacegroup_name, spacegroup_number = data_manipulation.read_structure_factor(targetPDBID_MTZ_format)
    # label, max_resolution, min_resolution,spacegroup_number, spacegroup_name = data_manipulation.read_data_from_MTZ(targetPDBID_MTZ_format)
    # print ("redirectedOutput", targetPDBID+"_" +diffraction_ID, spacegroup_number, spacegroup_name, label, min_resolution, max_resolution, placeHolder.rsplit(",")[2])
