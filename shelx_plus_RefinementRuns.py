from __future__ import division, print_function
import time, os, sys, glob, numpy as np
from itertools import count
import subprocess as sub
import shutil
from iotbx import crystal_symmetry_from_any
from iotbx.pdb import hierarchy
import functions.functions_library as func_lib
import data_generation_class.class_definitions as d_gen_class
import reflection_data_manipulation.reflection_data_manipulation_functions as data_manipulation


def runShelx_outsideClass(PDBID,
                          mtz_file,
                          sca_file,
                          target_resolution,
                          resolution_truncation,
                          substructure_atom_type,
                          starting_number_of_sites=None, on_TechneCluster=False):
    '''
    This function runs Shelx C/D/E pipeline starting from structure factors outside the class (to avoid using
    phasterng)
    '''
    crystal_data = crystal_symmetry_from_any.extract_from(mtz_file)
    # solvent_content = str(solvent_content)
    print("target resolution before adding a value", target_resolution)
    resolution = str(float(target_resolution) + float(resolution_truncation))
    unit_cell = str(crystal_data.unit_cell()).replace(",", "")
    unit_cell = unit_cell.replace("(", "")
    unit_cell = unit_cell.replace(")", "")
    spacegroup = str(crystal_data.space_group_info()).replace(" ", "")
    sca = sca_file
    # shelx_sampleScript = os.path.join("/home/ksh40/work/phassade/EP_class/default_scripts/shelxCD_only.sh")
    shelx_sampleScript = os.path.join(
        "/mnt/apollo/homes/ksh40/phassade/work/scripts/EP_class/default_scripts/shelxCD_only_forTechne.sh")

    shutil.copy2(shelx_sampleScript, "shelxCD_only.sh")  # copy the shelx script

    fin = open("shelxCD_only.sh", "rt")
    data = fin.read()
    data = data.replace('number_of_sites', str(starting_number_of_sites))
    data = data.replace('replace_input_data', sca)
    data = data.replace('replace_dimension', unit_cell)
    data = data.replace('replace_spacegroup', spacegroup)
    data = data.replace('replace_resolution', resolution)
    data = data.replace('replace_atomtype', substructure_atom_type)
    data = data.replace('replace_pdbid', PDBID)
    data = data.replace(':H', '')
    fin.close()

    fin = open("shelxCD_only.sh", "wt")
    fin.write(data)
    fin.close()

    # copyFunction = 'cp /home/ksh40/work/trial_runs/sampleScripts/shelx_sad.sh ' + self.PDBID + '.sh'
    # os.system(copyFunction)
    # # replace regular expressions
    # sedReplace = 'sed -i \'s/replace_pdbid/' + self.PDBID + '/\' ' + self.PDBID + '.sh'
    # os.system(sedReplace)
    # sedReplaceFa = 'sed -i \'s/replace_pdbid_fa/' + self.PDBID + '_fa/\' ' + self.PDBID + '.sh'
    # os.system(sedReplaceFa)
    # sedReplace = 'sed -i \'s/replace_solvent_content/' + solvent_content + '/\' ' + self.PDBID + '.sh'
    # os.system(sedReplace)
    # sedReplace = 'sed -i \'s/replace_resolution/' + resolution + '/\' ' + self.PDBID + '.sh'
    # os.system(sedReplace)
    # sedReplace = 'sed -i \'s/replace_dimension/' + unit_cell + '/\' ' + self.PDBID + '.sh'
    # os.system(sedReplace)
    # sedReplace = 'sed -i \'s/replace_spacegroup/' + spacegroup + '/\' ' + self.PDBID + '.sh'
    # os.system(sedReplace)
    # sedReplace = 'sed -i \'s/replace_atomtype/' + atom_type + '/\' ' + self.PDBID + '.sh'
    # os.system(sedReplace)
    # sedReplace = 'sed -i \'s/replace_input_data/' + sca + '/\' ' + self.PDBID + '.sh'
    # os.system(sedReplace)

    if on_TechneCluster:
        with open("shelx.log", 'w') as output:
            script = 'shelxCD_only.sh'
            shelx = sub.Popen(
                ['srun', script],
                # shell=True,
                # stdin=echo.stdout,
                stdout=output,
            )
        shelx.communicate()
    else:
        with open("shelx.log", 'w') as output:
            script = 'shelxCD_only.sh'
            shelx = sub.Popen(
                ['sh', script],
                # shell=True,
                # stdin=echo.stdout,
                stdout=output,
            )
        shelx.communicate()


if (__name__ == "__main__"):
    # func_lib.replace_atomType("/home/ksh40/temp/data/substructure.pdb", "I")
    # object_holder = d_gen_class.Phasing_methods("6JR4" + "_" + "w1",
    #                                             "/home/ksh40/temp/data/6JR4.pdb",
    #                                             "/home/ksh40/temp/data/substructure.pdb",
    #                                             "/home/ksh40/temp/data/6JR4.mtz",
    #                                             "/home/ksh40/temp/data/6JR4.fa",
    #                                             "AG", 2.0)
    #
    # object_holder.runEP_Auto()

    placeHolder = sys.argv[1]
    # print (placeHolder)
    targetPDBID = placeHolder.rsplit(",")[0].rsplit("_")[0]
    diffraction_ID = placeHolder.rsplit(",")[0].rsplit("_")[1]
    wavelength = float(placeHolder.rsplit(",")[1])
    substructure_atom_type = placeHolder.rsplit(",")[2].rsplit("/")[0]
    target_resolution = placeHolder.rsplit(",")[3]
    number_of_atoms = placeHolder.rsplit(",")[4]
    # number_of_sites = placeHolder.rsplit(",")[3]
    # substructure_atom_type = "AX"
    # f_prime = placeHolder.rsplit(",")[3]
    # f_double_prime = placeHolder.rsplit(",")[4]
    selected_atoms_occupancy = selected_atoms_occupancy_squared_sum = 0
    anomalous_scatterers_list = None
    substructure_determination_pipeline = True
    # substructure_determination_method = "shelx"

    sourcePath = os.path.join("/mnt/apollo/homes/ksh40/phassade/inputData/kau_dir", targetPDBID)  # run from techne
    destinationPath = os.path.join("/mnt/apollo/homes/ksh40/phassade/work/workingDir")  # run from techne

    ##########################
    ### running SAD determination
    ##########################

    if os.path.exists(sourcePath):
        os.chdir(sourcePath)
        ##########################
        ### running SAD Shelx
        ##########################
        tag = "shelx_"
        # for resolution_truncation in ["default_resolutionFull_100K_trial11"]:
        for resolution_truncation in ["default",
                                      "default_resolutionMinusOne_100K_trial6",
                                      "default_resolutionOneHalf_100K_trial11", "default_resolutionTwo_100K_trial9",
                                      "default_resolutionTwoHalf_100K_trial10"]:

            internalDir = tag + str(resolution_truncation)

            runDetails = "shelx_runs"  # if shelx then "shelx_runs
            # internalDir = "shelx_default"  # if shelx than appropriate sub-folder such as "shelx_default"

            if not os.path.exists(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID)):
                os.mkdir(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID))
                os.chdir(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID))
            else:
                os.chdir(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID))

            if not os.path.exists(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID, runDetails)):
                os.mkdir(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID, runDetails))
                os.chdir(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID, runDetails))
            else:
                os.chdir(os.path.join(destinationPath, targetPDBID + "_" + diffraction_ID, runDetails))

            reference_substructure = os.path.join(destinationPath,
                                                  targetPDBID + "_" + diffraction_ID,
                                                  "positive_control/"
                                                  "EP_SAD_withPerfectHa_fromRefinedModel_withoutLLGcompletion_fullResolution",
                                                  targetPDBID + "_" + diffraction_ID + ".1.pdb")

            print("path to reference substructure is ", reference_substructure, internalDir)

            ###creating dir to run shelx default ###
            if not os.path.exists(
                    os.path.join(destinationPath, str(targetPDBID) + "_" + str(diffraction_ID), runDetails, internalDir)):
                os.mkdir(
                    os.path.join(destinationPath, str(targetPDBID) + "_" + str(diffraction_ID), runDetails, internalDir))
                os.chdir(
                    os.path.join(destinationPath, str(targetPDBID) + "_" + str(diffraction_ID), runDetails, internalDir))
            else:
                os.chdir(
                    os.path.join(destinationPath, str(targetPDBID) + "_" + str(diffraction_ID), runDetails, internalDir))

            if substructure_determination_pipeline:  ### if shelx_pipeline is true
                print ("entering shelx pipeline", str(targetPDBID) + "_" + str(diffraction_ID), internalDir)
                # if not os.path.exists(
                #         os.path.join(destinationPath, str(targetPDBID) + "_" + str(diffraction_ID), runDetails, internalDir,
                #                      str(targetPDBID) + "_" + str(diffraction_ID) + "_fa.ins")): #testing if the job has already been run
                number_of_trials_run = func_lib.test_if_shelx_has_run_100kTrials(os.path.join(destinationPath, str(targetPDBID) + "_" +
                                                                       str(diffraction_ID),
                                                                       runDetails, internalDir, "shelx.log"))
                if number_of_trials_run < 100000:
                    # ## Running shelx outside the Phasing_methods class
                    if "default_resolutionFull_100K_trial11" in internalDir:
                        resolution_truncation_value = float (0.0)
                    elif "default" in internalDir:
                        resolution_truncation_value = float(0.5)
                    elif "default_resolutionMinusOne_100K_trial6" in internalDir:
                        resolution_truncation_value = float(1.0)
                    elif "default_resolutionOneHalf_100K_trial11" in internalDir:
                        resolution_truncation_value = float(1.5)
                    elif "default_resolutionTwo_100K_trial9" in internalDir:
                        resolution_truncation_value = float(2.0)
                    elif "default_resolutionTwoHalf_100K_trial10" in internalDir:
                        resolution_truncation_value = float(2.5)

                    runShelx_outsideClass(targetPDBID + "_" + diffraction_ID,
                                          os.path.join(sourcePath, diffraction_ID, targetPDBID +
                                                       "_" + diffraction_ID + ".mtz"),
                                          os.path.join(sourcePath, diffraction_ID, targetPDBID +
                                                       "_" + diffraction_ID + ".sca"),
                                          target_resolution,
                                          resolution_truncation_value,
                                          substructure_atom_type,
                                          starting_number_of_sites=number_of_atoms,
                                          on_TechneCluster=True)
                    print("run_Complete_redirectedOutput", placeHolder.rsplit(",")[0], selected_atoms_occupancy,
                          selected_atoms_occupancy_squared_sum, number_of_atoms, internalDir)
                else:
                    print("failed_to_run_redirectedOutput",placeHolder.rsplit(",")[0], internalDir)
            ## running shelx finished

            # ##########################
            # ### running hyss ###
            # ##########################
            # runDetails = "hyss_runs"
            # internalDir = "hyss_second_attempt"
            # if not os.path.exists(
            #         os.path.join(destinationPath, str(targetPDBID) + "_" + str(diffraction_ID), runDetails, internalDir)):
            #     os.mkdir(os.path.join(destinationPath, str(targetPDBID) + "_" + str(diffraction_ID), runDetails, internalDir))
            #     os.chdir(os.path.join(destinationPath, str(targetPDBID) + "_" + str(diffraction_ID), runDetails, internalDir))
            # else:
            #     os.chdir(os.path.join(destinationPath, str(targetPDBID) + "_" + str(diffraction_ID), runDetails, internalDir))
            #
            # if substructure_determination_pipeline:
            #     # Running hyss
            #     object_holder = d_gen_class.Phasing_methods(targetPDBID + "_" + diffraction_ID,
            #                                     os.path.join(sourcePath, targetPDBID + ".pdb"),
            #                                     os.path.join(sourcePath, targetPDBID + ".pdb"),
            #                                     os.path.join(sourcePath, diffraction_ID,
            #                                                  targetPDBID + "_" + diffraction_ID + ".mtz"),
            #                                     os.path.join(sourcePath, targetPDBID + ".fa"), substructure_atom_type,
            #                                     wavelength)
            #     object_holder.run_hyss(starting_number_of_sites=number_of_atoms)
            #     print ("run_Complete_redirectedOutput", placeHolder.rsplit(",")[0])
            # #### running hyss finished

            ##########################
            ### running SAD refinement
            ##########################
            else:  # if not structure determination pipeline then run SAD refinement
                hyss_PDBID = os.path.join(destinationPath, str(targetPDBID) + "_" + str(diffraction_ID), runDetails,
                                          internalDir,
                                          str(targetPDBID) + "_" + str(
                                              diffraction_ID) + "_fa.pdb")  # if hyss "_hyss_consensus_model.pdb" or if shelx "_fa.pdb"

                # print ("hyss_PDBID path is ", hyss_PDBID)

                if os.path.exists(hyss_PDBID) and (os.path.exists(reference_substructure)):
                    number_of_atoms = func_lib.generic_count_atomNumbers(hyss_PDBID)
                    # print("number_of_atoms is ", number_of_atoms)
                    if number_of_atoms > 0:

                        if not os.path.exists(
                                os.path.join(destinationPath, str(targetPDBID) + "_" + str(diffraction_ID), runDetails,
                                             internalDir, "refineDefault_LLGC")):
                            os.mkdir(os.path.join(destinationPath, str(targetPDBID) + "_" + str(diffraction_ID), runDetails,
                                                  internalDir, "refineDefault_LLGC"))
                            os.chdir(os.path.join(destinationPath, str(targetPDBID) + "_" + str(diffraction_ID), runDetails,
                                                  internalDir, "refineDefault_LLGC"))
                        else:
                            os.chdir(os.path.join(destinationPath, str(targetPDBID) + "_" + str(diffraction_ID), runDetails,
                                                  internalDir, "refineDefault_LLGC"))

                        #### setting target_mtz path
                        if os.path.exists(
                                os.path.join(sourcePath, diffraction_ID,
                                             targetPDBID + "_" + diffraction_ID + "_french_wilson.mtz")):
                            target_mtz = os.path.join(sourcePath, diffraction_ID,
                                                      targetPDBID + "_" + diffraction_ID + "_french_wilson.mtz")
                        else:
                            target_mtz = os.path.join(sourcePath, diffraction_ID,
                                                      targetPDBID + "_" + diffraction_ID + ".mtz")

                        ### running EP_AUTO SAD refinement
                        object_holder = d_gen_class.Phasing_methods(targetPDBID + "_" + diffraction_ID,
                                                                    os.path.join(sourcePath, targetPDBID + ".pdb"),
                                                                    hyss_PDBID,
                                                                    target_mtz,
                                                                    os.path.join(sourcePath, targetPDBID + ".fa"),
                                                                    substructure_atom_type, wavelength)

                        object_holder.runEP_Auto()
                        print("run_Complete_redirectedOutput",placeHolder.rsplit(",")[0], internalDir)

                        ##setting paths of SAD refined files
                        SAD_PDBID = os.path.join(targetPDBID + "_" + diffraction_ID + ".1.pdb")
                        SAD_log = os.path.join(targetPDBID + "_" + diffraction_ID + ".log")


                        ## print ("SAD_PDBID file is", SAD_PDBID)
                        ## print("SAD_PDBID file is", os.getcwd())

                        if os.path.exists(SAD_PDBID):
                            # # # ###Using pre-refined model, calculate occupancy auditing after comparison using emma
                            ### running from previous dir
                            os.chdir(os.path.join(destinationPath, str(targetPDBID) + "_" + str(diffraction_ID), runDetails,
                                                  internalDir))
                            emma_rotation_preRefined, emma_rmsd_preRefined, anomalous_scatterers_list_preRefined = func_lib.occupancy_audit_version2(
                                                                                                reference_substructure,
                                                                                                hyss_PDBID,
                                                                                                substructure_atom_type,
                                                                                                3.0,
                                                                                                output_file_name=False,
                                                                                                output_occupancy_list=True)

                            print("XXXXXXXXXXXXXXXX\n")
                            for rowArray in anomalous_scatterers_list_preRefined:
                                print("redirectedOutputPreRefined_occAudit",targetPDBID + "_" + diffraction_ID,
                                      rowArray[0], rowArray[1], rowArray[2],
                                      rowArray[3], internalDir)

                            ### running from within the refineDefault_LLGC
                            os.chdir(os.path.join(destinationPath, str(targetPDBID) + "_" + str(diffraction_ID), runDetails,
                                                  internalDir, "refineDefault_LLGC"))

                            # # # ### calculate occupancy auditing after comparison using emma
                            emma_rotation, emma_rmsd, anomalous_scatterers_list = func_lib.occupancy_audit_version2(reference_substructure,
                                                                                                     SAD_PDBID,
                                                                                                     substructure_atom_type,
                                                                                                     3.0,
                                                                                                     output_file_name=False,
                                                                                                     output_occupancy_list=True)

                            ###if rotation matrix is opposite hand
                            if str("{-1, 0, 0}, {0, -1, 0}, {0, 0, -1}") in emma_rotation:
                                SAD_MTZ = os.path.join(str(targetPDBID) + "_" + str(diffraction_ID) + ".1.hand.mtz")
                            else:
                                SAD_MTZ = os.path.join(str(targetPDBID) + "_" + str(diffraction_ID) + ".1.mtz")

                            ###calculating map-model CC
                            localCC = d_gen_class.Substructure.runCC_MTZ_PDB(SAD_MTZ,
                                                                             os.path.join(sourcePath, targetPDBID + ".pdb"))

                            print("redirectedOutput_localCC", targetPDBID + "_" + diffraction_ID, localCC,
                                  emma_rmsd_preRefined,
                                  emma_rmsd, internalDir)

                            atom_f_double_f_prime_list = np.array(func_lib.read_EP_SAD_log(SAD_log))
                            print("atom_f_double_f_prime_list is :", atom_f_double_f_prime_list, internalDir)

                            # print (anomalous_scatterers_list)
                            if anomalous_scatterers_list is not None:
                                if anomalous_scatterers_list.ndim > 1:
                                    for f in anomalous_scatterers_list:
                                        if f[0] == atom_f_double_f_prime_list[0]:
                                            print("redirectedOutputRefined_occAudit", targetPDBID + "_" + diffraction_ID,
                                                  f[0], f[1], f[2], f[3], atom_f_double_f_prime_list[0],
                                                  atom_f_double_f_prime_list[1],
                                                  atom_f_double_f_prime_list[2], internalDir)
                                        else:
                                            print("redirectedOutputRefined_occAudit", targetPDBID + "_" + diffraction_ID,
                                                  f[0], f[1], f[2], f[3], internalDir)
                                else:
                                    if anomalous_scatterers_list[0] == atom_f_double_f_prime_list[0]:
                                        print("redirectedOutputRefined_occAudit",internalDir,
                                              targetPDBID + "_" + diffraction_ID,
                                              anomalous_scatterers_list[0],
                                              anomalous_scatterers_list[1],
                                              anomalous_scatterers_list[2],
                                              anomalous_scatterers_list[3],
                                              atom_f_double_f_prime_list[0],
                                              atom_f_double_f_prime_list[1],
                                              atom_f_double_f_prime_list[2], internalDir
                                              )
                                    else:
                                        print("redirectedOutputRefined_occAudit",internalDir,
                                              targetPDBID + "_" + diffraction_ID,
                                              anomalous_scatterers_list[0],
                                              anomalous_scatterers_list[1],
                                              anomalous_scatterers_list[2],
                                              anomalous_scatterers_list[3], internalDir)
                            else:
                                print("failed_redirectedOutput_emmaNoMatch", targetPDBID + "_" + diffraction_ID,
                                      internalDir)

                            shutil.rmtree("./temp_dir", ignore_errors=True)
                else:
                    print("failed_redirectedOutput_zero_or_fileDoesntExist", targetPDBID + "_" + diffraction_ID,
                          internalDir)

            tag = "shelx_"
